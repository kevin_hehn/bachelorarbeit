package com.example.hehn.bachelorarbeit.database;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.hehn.bachelorarbeit.Menu.Main_Menu;
import com.example.hehn.bachelorarbeit.MyAdapter;
import com.example.hehn.bachelorarbeit.R;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Catalogue_Selection extends AppCompatActivity {


        // Progress Dialog
        private ProgressDialog pDialog;

        // Creating JSON Parser object
        JSONParser jParser = new JSONParser();

        ArrayList<HashMap<String, String>> productsList;

        // url to get all products list
        String url_all_products = "http://192.168.1.124:8080/bachelorarbeit/home.php";
        ListView list;
        // products JSONArray
        JSONArray products = null;
        Map<String, String> myMap = new HashMap<String, String>();


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_catalogue__selection);
            list = (ListView) findViewById(R.id.listView);
            productsList = new ArrayList<HashMap<String, String>>();

            // Loading products in Background Thread
            new LoadAllProducts().execute();

        }
        class LoadAllProducts extends AsyncTask<String, String, String> {

            /**
             * Before starting background thread Show Progress Dialog
             * */
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                pDialog = new ProgressDialog(Catalogue_Selection.this);
                pDialog.setMessage(getString(R.string.catalogue_loading));
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(false);
                pDialog.show();
            }

            /**
             * getting All products from url
             * */
            protected String doInBackground(String... args) {
                // Building Parameters


                List<NameValuePair> params = new ArrayList<NameValuePair>();

                String url_all_products = "http://diocles.informatik.uni-mannheim.de/ba_hehn/catalogues.php";
                // getting JSON string from URL
                JSONObject json = jParser.makeHttpRequest(url_all_products, "GET", params);

                if(jParser.makeHttpRequest(url_all_products, "GET", params)==null){
                    return null;
                }

                json = jParser.makeHttpRequest(url_all_products, "GET", params);
                // Check your log cat for JSON reponse
                Log.d("All Products: ", json.toString());
                try {
                    products = json.getJSONArray("answers");
                    for (int i = 0; i < products.length(); i++) {
                        JSONObject c = products.getJSONObject(i);
                        String id = c.getString("id");
                        String description = c.getString("description");
                        myMap.put(id,description);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            /**
             * After completing background task Dismiss the progress dialog
             * **/
            protected void onPostExecute(String file_url) {
                // dismiss the dialog after getting all products
                pDialog.dismiss();
                // updating UI from Background Thread
                MyAdapter adapter = new MyAdapter(myMap);
                list.setAdapter(adapter);
               if (list.getCount()==0){
                   Intent myIntent = new Intent(Catalogue_Selection.this, Main_Menu.class);
                   Catalogue_Selection.this.startActivity(myIntent);
               }


                list.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?>adapter,View v, int position,long a){
                        Map.Entry<String, String> item = (Map.Entry<String, String>) adapter.getItemAtPosition(position);
                        String key = item.getKey();
                        Log.d("key",key);
                        Intent intent = new Intent(Catalogue_Selection.this,Database_Connection.class);
                        intent.putExtra("id",key);
                        startActivity(intent);
                    }


                });

                //Intent myIntent = new Intent(Catalogue_Selection.this, Database_Connection.class);
                //Catalogue_Selection.this.startActivity(myIntent);
            }

        }

}
