package com.example.hehn.bachelorarbeit.Menu;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.EmbossMaskFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.androidplot.pie.PieChart;
import com.androidplot.pie.Segment;
import com.androidplot.pie.SegmentFormatter;
import com.example.hehn.bachelorarbeit.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Profile_Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Profile_Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Profile_Fragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    TextView tWon;
    TextView tLost;
    TextView tWon2;
    TextView tLost2;
    TextView drawTextview;
    TextView playedGames;
    TextView score_Textview;
    private PieChart pie;
    private PieChart pie2;
    int won;
    int lost;
    int draw;
    int rightQuestions;
    int wrongQuestions;
    private Segment s1;
    private Segment s2;
    private Segment s3;
    private Segment s4;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Profile_Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Profile_Fragment newInstance(String param1, String param2) {
        Profile_Fragment fragment = new Profile_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public Profile_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FrameLayout FL = (FrameLayout)inflater.inflate(R.layout.fragment_profile_, container, false);
        this.tWon = (TextView) FL.findViewById(R.id.wins);
        tLost = (TextView) FL.findViewById(R.id.lost);

        this.tWon2 = (TextView) FL.findViewById(R.id.won2);
        tLost2 = (TextView) FL.findViewById(R.id.lost2);
        drawTextview = (TextView) FL.findViewById(R.id.drawTextview);

        this.playedGames = (TextView) FL.findViewById(R.id.overall_Textview);

        this.score_Textview = (TextView) FL.findViewById(R.id.score_Textview);

        pie = (PieChart) FL.findViewById(R.id.mySimplePieChart);
        pie2 = (PieChart) FL.findViewById(R.id.mySimplePieChart2);

        Log.d("Profile", tWon.getText().toString());

        return FL;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public void onStart(){
        super.onStart();
        SQLiteDatabase mydatabase = getActivity().openOrCreateDatabase("StatisticDB", getActivity().MODE_PRIVATE, null);
        mydatabase.execSQL("CREATE TABLE IF NOT EXISTS Questions (right bit, wrong bit)");
        mydatabase.execSQL("CREATE TABLE IF NOT EXISTS games (level int,date date,won int)");



        won = getWonGames();
        lost = getLostGames();
        draw = getDrawGames();

        int overallGames  = won+lost+draw;
        rightQuestions = getRightQuestions();
        wrongQuestions = getWrongQuestions();
        //Log.d("ProfileOnResume", Integer.toString(won) + Integer.toString(lost));

        playedGames.setText(getString(R.string.profile_overall_1)+" "+Integer.toString(overallGames)+" "+getString(R.string.profile_overall_2));
        score_Textview.setText(getString(R.string.profile_score)+" "+Integer.toString(getScores()));
        tWon.setText(getString(R.string.profile_won)+" " + Integer.toString(won));
        tLost.setText(getString(R.string.profile_lost)+" "+Integer.toString(lost));
        drawTextview.setText(getString(R.string.profile_draw)+" "+Integer.toString(draw));

        tWon2.setText(getString(R.string.profile_right)+" "+Integer.toString(rightQuestions));
        tLost2.setText(getString(R.string.profile_wrong)+" "+Integer.toString(wrongQuestions));

        openChart();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public int getWonGames(){
        SQLiteDatabase mydatabase = getActivity().openOrCreateDatabase("StatisticDB", getActivity().MODE_PRIVATE, null);
        Cursor resultSet = mydatabase.rawQuery("SELECT COUNT(*) FROM games WHERE won = 1", null);
        resultSet.moveToFirst();
        int won  = resultSet.getInt(0);
        resultSet.close();
        return won;
    }
    public int getLostGames(){
        SQLiteDatabase mydatabase = getActivity().openOrCreateDatabase("StatisticDB", getActivity().MODE_PRIVATE,null);
        Cursor resultSet = mydatabase.rawQuery("SELECT COUNT(*) FROM games WHERE won = 0", null);
        resultSet.moveToFirst();
        int won  = resultSet.getInt(0);
        resultSet.close();
        return won;
    }
    public int getDrawGames(){
        SQLiteDatabase mydatabase = getActivity().openOrCreateDatabase("StatisticDB", getActivity().MODE_PRIVATE,null);
        Cursor resultSet = mydatabase.rawQuery("SELECT COUNT(*) FROM games WHERE won = -1", null);
        resultSet.moveToFirst();
        int won  = resultSet.getInt(0);
        resultSet.close();
        return won;
    }
    public int getRightQuestions(){
        SQLiteDatabase mydatabase = getActivity().openOrCreateDatabase("StatisticDB", getActivity().MODE_PRIVATE,null);
        Cursor resultSet = mydatabase.rawQuery("SELECT COUNT(*) FROM Questions WHERE right = 1", null);
        resultSet.moveToFirst();
        int won  = resultSet.getInt(0);
        resultSet.close();
        return won;
    }
    public int getWrongQuestions(){
        SQLiteDatabase mydatabase = getActivity().openOrCreateDatabase("StatisticDB", getActivity().MODE_PRIVATE,null);
        Cursor resultSet = mydatabase.rawQuery("SELECT COUNT(*) FROM Questions WHERE right = 0", null);
        resultSet.moveToFirst();
        int won  = resultSet.getInt(0);
        resultSet.close();
        return won;
    }
    public int getScores(){
        int score=0;
        int won;
        int lost;
        SQLiteDatabase mydatabase = getActivity().openOrCreateDatabase("StatisticDB", getActivity().MODE_PRIVATE,null);
        Cursor resultSet = mydatabase.rawQuery("SELECT COUNT(*) FROM games WHERE level = 0 AND won = 1", null);
        resultSet.moveToFirst();
        won  = resultSet.getInt(0);
        resultSet.close();

        score = score + (won*2);

        mydatabase = getActivity().openOrCreateDatabase("StatisticDB", getActivity().MODE_PRIVATE, null);
        resultSet = mydatabase.rawQuery("SELECT COUNT(*) FROM games WHERE level = 1 AND won = 1", null);
        resultSet.moveToFirst();
        won  = resultSet.getInt(0);
        resultSet.close();

        score = score + (won*4);

        mydatabase = getActivity().openOrCreateDatabase("StatisticDB", getActivity().MODE_PRIVATE, null);
        resultSet = mydatabase.rawQuery("SELECT COUNT(*) FROM games WHERE level = 2 AND won = 1", null);
        resultSet.moveToFirst();
        won  = resultSet.getInt(0);
        resultSet.close();

        score = score + (won*6);


        mydatabase = getActivity().openOrCreateDatabase("StatisticDB", getActivity().MODE_PRIVATE, null);
        resultSet = mydatabase.rawQuery("SELECT COUNT(*) FROM games WHERE level = 0 AND won = 0", null);
        resultSet.moveToFirst();
        lost  = resultSet.getInt(0);
        resultSet.close();

        score = score - (lost*3);

        mydatabase = getActivity().openOrCreateDatabase("StatisticDB", getActivity().MODE_PRIVATE, null);
        resultSet = mydatabase.rawQuery("SELECT COUNT(*) FROM games WHERE level = 1 AND won = 0", null);
        resultSet.moveToFirst();
        lost  = resultSet.getInt(0);
        resultSet.close();

        score = score - (lost*2);

        mydatabase = getActivity().openOrCreateDatabase("StatisticDB", getActivity().MODE_PRIVATE, null);
        resultSet = mydatabase.rawQuery("SELECT COUNT(*) FROM games WHERE level = 2 AND won = 0", null);
        resultSet.moveToFirst();
        lost  = resultSet.getInt(0);
        resultSet.close();

        score = score - (lost*1);

        SharedPreferences sharedpreferences;
        sharedpreferences = this.getActivity().getSharedPreferences("MyPREFERENCES", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.remove("score");
        editor.commit();
        String level = "";
        if(score < 0)
            level = "0";
        if(score >= 0 && score <50)
            level = "1";
        if(score >= 50)
            level = "2";
        editor.putString("score", level);
        editor.commit();

        return score;
    }


    private void openChart() {
        s1 = new Segment(getString(R.string.profile_won_s), won);
        s2 = new Segment(getString(R.string.profile_lost_s), lost);
        s3 = new Segment(getString(R.string.profile_draw_s), draw);

        EmbossMaskFilter emf = new EmbossMaskFilter(
                new float[]{1, 1, 1}, 0.4f, 10, 8.2f);

        SegmentFormatter sf1 = new SegmentFormatter();
        sf1.configure(getActivity(), R.xml.pie_segment_formatter2);

        sf1.getFillPaint().setMaskFilter(emf);

        SegmentFormatter sf2 = new SegmentFormatter();
        sf2.configure(getActivity(), R.xml.pie_segment_formatter1);

        sf2.getFillPaint().setMaskFilter(emf);

        SegmentFormatter sf3 = new SegmentFormatter();
        sf3.configure(getActivity(), R.xml.pie_segment_formatter3);

        sf3.getFillPaint().setMaskFilter(emf);

        //SegmentFormatter sf4 = new SegmentFormatter();
        //sf4.configure(getActivity(), R.xml.pie_segment_formatter1);

        //sf4.getFillPaint().setMaskFilter(emf);

        pie.addSeries(s1, sf1);
        pie.addSeries(s2, sf2);
        pie.addSeries(s3, sf3);
        //pie.addSeries(s4, sf4);

        pie.getBorderPaint().setColor(Color.TRANSPARENT);
        pie.getBackgroundPaint().setColor(Color.TRANSPARENT);

        //pie.addSeries(s4, sf4);






        s1 = new Segment(getString(R.string.profile_right_s), rightQuestions);
        s2 = new Segment(getString(R.string.profile_wrong_s), wrongQuestions);

         emf = new EmbossMaskFilter(
                new float[]{1, 1, 1}, 0.4f, 10, 8.2f);

         sf1 = new SegmentFormatter();
        sf1.configure(getActivity(), R.xml.pie_segment_formatter2);

        sf1.getFillPaint().setMaskFilter(emf);

         sf2 = new SegmentFormatter();
        sf2.configure(getActivity(), R.xml.pie_segment_formatter1);

        sf2.getFillPaint().setMaskFilter(emf);


        pie2.addSeries(s1, sf1);
        pie2.addSeries(s2, sf2);

        pie2.getBorderPaint().setColor(Color.TRANSPARENT);
        pie2.getBackgroundPaint().setColor(Color.TRANSPARENT);


    }

    public void onResume(){
        super.onResume();
        // Set title bar
        ((Main_Menu) getActivity()).setActionBarTitle("Statistics");
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }






}
