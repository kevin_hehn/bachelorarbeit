package com.example.hehn.bachelorarbeit.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Created by Hehn on 23.01.2016.
 */
public class database extends SQLiteOpenHelper {

    public SQLiteDatabase db;
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "contactsManager";

    // Contacts table name
    private static final String TABLE_CONTACTS = "contacts";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_PH_NO = "phone_number";

    public database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
       // db = new SQLiteDatabase();
    }



    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL("CREATE TABLE IF NOT EXISTS Questions(ID int,Question VARCHAR);");
        database.execSQL("CREATE TABLE IF NOT EXISTS Answers(ID int,Answer_1 VARCHAR,Answer_2 VARCHAR,Answer_3 VARCHAR,Answer_4 VARCHAR,Answer_right int);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(database.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS Questions");
        db.execSQL("DROP TABLE IF EXISTS Answers");
        onCreate(db);
    }


/*
    public void initializeDB() {
//        deleteDatabase("QuestionDB");
        mydatabase = openOrCreateDatabase("QuestionDB", MODE_PRIVATE, null);
        mydatabase.execSQL("CREATE TABLE IF NOT EXISTS Questions(ID int,Question VARCHAR);");
        mydatabase.execSQL("CREATE TABLE IF NOT EXISTS Answers(ID int,Answer_1 VARCHAR,Answer_2 VARCHAR,Answer_3 VARCHAR,Answer_4 VARCHAR,Answer_right int);");

        mydatabase.execSQL("INSERT INTO Questions VALUES('1','How can you call the police in germany?');");
        mydatabase.execSQL("INSERT INTO Answers VALUES('1','110','113','114','115',0);");

        mydatabase.execSQL("INSERT INTO Questions VALUES('2','What can you say to great people?');");
        mydatabase.execSQL("INSERT INTO Answers VALUES('2','Tschüss','Hallo','Polizei','Hund',1);");

        mydatabase.execSQL("INSERT INTO Questions VALUES('3','Where can you get medical help from a doctor?');");
        mydatabase.execSQL("INSERT INTO Answers VALUES('3','Krankenhaus','REWE','Apotheke','Universität',0);");

        mydatabase.execSQL("INSERT INTO Questions VALUES('4','Before you enter a bus please make sure to:');");
        mydatabase.execSQL("INSERT INTO Answers VALUES('4','put off your shoes','buy a ticket','eat something','sing',1);");

        mydatabase.execSQL("INSERT INTO Questions VALUES('5','What is car in german?');");
        mydatabase.execSQL("INSERT INTO Answers VALUES('5','Auto','Dummy','Test','X',0);");
    }
    public int getQuestionID(){
        Cursor resultSet = mydatabase.rawQuery("Select max(ID) from Questions", null);
        resultSet.moveToFirst();
        int maxID = resultSet.getInt(0);
        int randomID = (int)(Math.random()*maxID+1);
        resultSet.close();
        return  randomID;
    }
    public String getQuestion(int ID){
        Cursor resultSet = mydatabase.rawQuery("Select * from Questions WHERE ID = " + ID + "", null);
        resultSet.moveToFirst();
        String Question = resultSet.getString(1);
        resultSet.close();
        return Question;
    }
    public String[] getAnswer(int ID){
        String[] temp = new String[4];
        Cursor resultSet = mydatabase.rawQuery("Select * from Answers WHERE ID = "+ID+"", null);
        resultSet.moveToFirst();
        temp[0] = resultSet.getString(1);
        temp[1] = resultSet.getString(2);
        temp[2] = resultSet.getString(3);
        temp[3] = resultSet.getString(4);
        resultSet.close();
        return temp;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }*/
}
