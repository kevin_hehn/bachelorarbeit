package com.example.hehn.bachelorarbeit.database;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;


import com.example.hehn.bachelorarbeit.Menu.Main_Menu;
import com.example.hehn.bachelorarbeit.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Database_Connection extends AppCompatActivity {


    // Progress Dialog
    private ProgressDialog pDialog;

    // Creating JSON Parser object
    JSONParser jParser = new JSONParser();

    ArrayList<HashMap<String, String>> productsList;

    // url to get all products list
    String url_all_products = "http://192.168.1.124:8080/bachelorarbeit/home.php";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCTS = "products";
    private static final String TAG_PID = "pid";
    private static final String TAG_NAME = "name";

    // products JSONArray
    JSONArray products = null;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database__connection);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        productsList = new ArrayList<HashMap<String, String>>();
        Bundle IntentBundle = getIntent().getExtras();
        id = IntentBundle.getString("id");
        // Loading products in Background Thread
        new LoadAllProducts().execute();

    }
    class LoadAllProducts extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(Database_Connection.this);
            pDialog.setMessage(getString(R.string.catalogue_loading));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting All products from url
         * */
        protected String doInBackground(String... args) {
            // Building Parameters


            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("id",id));
;



            String url_all_products = "http://diocles.informatik.uni-mannheim.de/ba_hehn/home.php";
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(url_all_products, "GET", params);

            if(jParser.makeHttpRequest(url_all_products, "GET", params)==null){
                return null;
            }


            deleteDatabase("QuestionDB");
            SQLiteDatabase mydatabase = openOrCreateDatabase("QuestionDB",MODE_PRIVATE,null);
            mydatabase.execSQL("CREATE TABLE answers ( id int NOT NULL, " +
                    "question_id int NOT NULL, " +
                    "answer VARCHAR NOT NULL, " +
                    "correct int NOT NULL, " +
                    "author_id int)");

            mydatabase.execSQL("CREATE TABLE questions (id int NOT NULL, " +
                    "question VARCHAR NOT NULL, " +
                    "difficulty int NOT NULL, " +
                    "catalogue_id int NOT NULL, " +
                    "author_id int)");


            url_all_products = "http://diocles.informatik.uni-mannheim.de/ba_hehn/home.php";
            // getting JSON string from URL
            json = jParser.makeHttpRequest(url_all_products, "GET", params);
            // Check your log cat for JSON reponse
            Log.d("All Products: ", json.toString());
            try {
                    products = json.getJSONArray("answers");
                    for (int i = 0; i < products.length(); i++) {
                        JSONObject c = products.getJSONObject(i);
                        String id = c.getString("id");
                        String author_id = c.getString("author_id");
                        String difficulty = c.getString("difficulty");
                        String question = c.getString("question");
                        String catalogue_id = c.getString("catalogue_id");
                        mydatabase.execSQL("INSERT INTO questions VALUES("+id+", '"+question+"', "+difficulty+", "+catalogue_id+", "+author_id+")");
                    }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            url_all_products = "http://diocles.informatik.uni-mannheim.de/ba_hehn/answer.php";
            // getting JSON string from URL
            json = jParser.makeHttpRequest(url_all_products, "GET", params);
            // Check your log cat for JSON reponse
            Log.d("All Products: ", json.toString());
            try {
                products = json.getJSONArray("answers");
                for (int i = 0; i < products.length(); i++) {
                    JSONObject c = products.getJSONObject(i);
                    String id = c.getString("id");
                    String question_id = c.getString("question_id");
                    String answer = c.getString("answer");
                    answer = answer.replace("'","''");
                    String correct = c.getString("correct");
                    String author_id = c.getString("author_id");
                    Log.d("dd",id);
                    Log.d("dd",question_id);
                    Log.d("dd",answer);
                    Log.d("dd",correct);
                    Log.d("dd",author_id);
                    mydatabase.execSQL("INSERT INTO answers VALUES(" + id + ", " + question_id + ", '" + answer + "', " + correct + ", " + author_id + ")");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            mydatabase = openOrCreateDatabase("StatisticDB",MODE_PRIVATE,null);
            mydatabase.execSQL("CREATE TABLE IF NOT EXISTS Questions (right bit, wrong bit)");
            mydatabase.execSQL("CREATE TABLE IF NOT EXISTS games (level int,date date,won int)");
            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            pDialog.dismiss();
            // updating UI from Background Thread
            Intent myIntent = new Intent(Database_Connection.this, Main_Menu.class);
            Database_Connection.this.startActivity(myIntent);
        }

    }


}
