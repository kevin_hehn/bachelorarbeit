package com.example.hehn.bachelorarbeit.Menu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.hehn.bachelorarbeit.R;
import com.example.hehn.bachelorarbeit.Spiel.Bohnenspiel_2;
import com.example.hehn.bachelorarbeit.Spiel.Bohnenspiel_Hotseat;
import com.example.hehn.bachelorarbeit.Spiel.Bohnenspiel_Mario;
import com.example.hehn.bachelorarbeit.Spiel.Text_Activity;
import com.example.hehn.bachelorarbeit.database.ImageDB;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Home_Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Home_Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Home_Fragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    SharedPreferences sharedpreferences;

    public Spinner spinner;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    Button startButton;
    Button startHotseatButton;
    Button startImageButton;
    Button startMarioButton;
    Button startTextButton;
    String tempBohnen;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Home_Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Home_Fragment newInstance(String param1, String param2) {
        Home_Fragment fragment = new Home_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public Home_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    public void createQuestionDB(){
        /*Context context = getActivity();
        context.deleteDatabase("QuestionDB");
        SQLiteDatabase mydatabase = context.openOrCreateDatabase("QuestionDB", context.MODE_PRIVATE, null);
        mydatabase.execSQL("CREATE TABLE IF NOT EXISTS Questions(ID int,Question VARCHAR);");
        mydatabase.execSQL("CREATE TABLE IF NOT EXISTS Answers(ID int,Answer_1 VARCHAR,Answer_2 VARCHAR,Answer_3 VARCHAR,Answer_4 VARCHAR,Answer_right int);");

        mydatabase.execSQL("INSERT INTO Questions VALUES('1','How can you call the police in germany?');");
        mydatabase.execSQL("INSERT INTO Answers VALUES('1','110','113','114','115',0);");

        mydatabase.execSQL("INSERT INTO Questions VALUES('2','What can you say to great people?');");
        mydatabase.execSQL("INSERT INTO Answers VALUES('2','Tschüss','Hallo','Polizei','Hund',1);");

        mydatabase.execSQL("INSERT INTO Questions VALUES('3','Where can you get medical help from a doctor?');");
        mydatabase.execSQL("INSERT INTO Answers VALUES('3','Krankenhaus','REWE','Apotheke','Universität',0);");

        mydatabase.execSQL("INSERT INTO Questions VALUES('4','Before you enter a bus please make sure to:');");
        mydatabase.execSQL("INSERT INTO Answers VALUES('4','put off your shoes','buy a ticket','eat something','sing',1);");

        mydatabase.execSQL("INSERT INTO Questions VALUES('5','What is car in german?');");
        mydatabase.execSQL("INSERT INTO Answers VALUES('5','Auto','Dummy','Test','X',0);");*/

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        FrameLayout FL = (FrameLayout)inflater.inflate(R.layout.fragment_home_, container, false);
        startButton = (Button) FL.findViewById(R.id.start_button);
        spinner  = (Spinner) FL.findViewById(R.id.spinner1);

        sharedpreferences = this.getActivity().getSharedPreferences("MyPREFERENCES", Context.MODE_PRIVATE);
        tempBohnen = sharedpreferences.getString("bohnen", "2");
        String tempScore = sharedpreferences.getString("score", "0");
        spinner.setSelection(Integer.parseInt(tempScore));

        startButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int temp = spinner.getSelectedItemPosition();
                Log.d("SpinnerPosition", Integer.toString(temp));
                Intent intent = new Intent(v.getContext(), Bohnenspiel_2.class);
                intent.putExtra("level", temp);
                intent.putExtra("bohnen", tempBohnen);
                startActivity(intent);

            }
        });
        startHotseatButton = (Button) FL.findViewById(R.id.start_button_hotseat);
        startHotseatButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), Bohnenspiel_Hotseat.class);
                intent.putExtra("level",1);
                intent.putExtra("bohnen",tempBohnen);
                startActivity(intent);

            }
        });
        startImageButton = (Button) FL.findViewById(R.id.start_button_image);
        startImageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int temp = spinner.getSelectedItemPosition();
                Intent intent = new Intent(v.getContext(), ImageDB.class);
                intent.putExtra("bohnen",tempBohnen);
                intent.putExtra("level",temp);
                startActivity(intent);

            }
        });

        startMarioButton = (Button) FL.findViewById(R.id.start_button_mario);
        startMarioButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int temp = spinner.getSelectedItemPosition();
                Intent intent = new Intent(v.getContext(), Bohnenspiel_Mario.class);
                intent.putExtra("level",temp);
                intent.putExtra("bohnen",tempBohnen);
                startActivity(intent);
            }
        });

        startTextButton = (Button) FL.findViewById(R.id.text_button);
        startTextButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), Text_Activity.class);
                startActivity(intent);
            }
        });

        //Toast.makeText(getActivity(), tempBohnen,
        //        Toast.LENGTH_LONG).show();




        return FL;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    public void onResume(){
        super.onResume();
        ((Main_Menu) getActivity()).setActionBarTitle("Game");
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
