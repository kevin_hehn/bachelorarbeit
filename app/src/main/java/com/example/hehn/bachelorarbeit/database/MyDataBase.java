package com.example.hehn.bachelorarbeit.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Hehn on 29.02.2016.
 */
public class MyDataBase extends SQLiteOpenHelper {
        public MyDataBase(Context context, String dbname, SQLiteDatabase.CursorFactory factory, int dbversion) {
            super(context, dbname, factory, dbversion);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            //db.execSQL("drop table imageQuestion;");
            db.execSQL("create table imageQuestion(id INT,question VARCHAR NOT NULL,right VARCHAR NOT NULL,image_a blob,image_b blob,image_c blob,image_d blob);");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
}
