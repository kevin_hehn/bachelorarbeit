package com.example.hehn.bachelorarbeit.database;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ImageView;

import com.example.hehn.bachelorarbeit.R;
import com.example.hehn.bachelorarbeit.Spiel.BohnenSpiel_ImageMode;

import java.io.ByteArrayOutputStream;

public class ImageDB extends AppCompatActivity {

    private ImageView imageview=null;
    private Button btninsert=null;
    private Button btnretrive=null;
    private MyDataBase mdb=null;
    private SQLiteDatabase db=null;
    private Cursor c=null;
    private byte[] img_a=null;
    private byte[] img_b=null;
    private byte[] img_c=null;
    private byte[] img_d=null;
    private static final String DATABASE_NAME = "ImageDb.db";
    public static final int DATABASE_VERSION = 1;
    int temp;
    String tempBohnen;
    Bundle IntentBundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_db);
        IntentBundle = getIntent().getExtras();
        temp = IntentBundle.getInt("level");
        tempBohnen = IntentBundle.getString("bohnen");
        //btninsert=(Button)findViewById(R.id.button_insert);
        //btnretrive= (Button)findViewById(R.id.button_retrieve);
        //imageview= (ImageView)findViewById(R.id.imageView_image);
        //imageview.setImageResource(0);
        //btninsert.setOnClickListener(this);
        //btnretrive.setOnClickListener(this);
        getApplicationContext().deleteDatabase(DATABASE_NAME);
        mdb=new MyDataBase(getApplicationContext(), DATABASE_NAME,null, DATABASE_VERSION);


        //Apfel
        Bitmap a= BitmapFactory.decodeResource(getResources(), R.drawable.apfel);
        ByteArrayOutputStream aos=new ByteArrayOutputStream();
        a.compress(Bitmap.CompressFormat.PNG, 100, aos);
        img_a=aos.toByteArray();

        Bitmap b= BitmapFactory.decodeResource(getResources(), R.drawable.birne);
        ByteArrayOutputStream bos=new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.PNG, 100, bos);
        img_b=bos.toByteArray();

        Bitmap c= BitmapFactory.decodeResource(getResources(), R.drawable.erdbeere);
        ByteArrayOutputStream cos=new ByteArrayOutputStream();
        c.compress(Bitmap.CompressFormat.PNG, 100, cos);
        img_c=cos.toByteArray();

        Bitmap d= BitmapFactory.decodeResource(getResources(), R.drawable.himbeere);
        ByteArrayOutputStream dos=new ByteArrayOutputStream();
        d.compress(Bitmap.CompressFormat.PNG, 100, dos);
        img_d=dos.toByteArray();

        db=mdb.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put("question", "Apfel");
        cv.put("right", "img_a");
        cv.put("id", 0);
        cv.put("image_a", img_a);
        cv.put("image_b", img_b);
        cv.put("image_c", img_c);
        cv.put("image_d", img_d);
        db.insert("imageQuestion", null, cv);

        //Auto
        a= BitmapFactory.decodeResource(getResources(), R.drawable.auto);
        aos=new ByteArrayOutputStream();
        a.compress(Bitmap.CompressFormat.PNG, 100, aos);
        img_a=aos.toByteArray();

        b= BitmapFactory.decodeResource(getResources(), R.drawable.apfel);
        bos=new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.PNG, 100, bos);
        img_b=bos.toByteArray();

        c= BitmapFactory.decodeResource(getResources(), R.drawable.fahrrad);
        cos=new ByteArrayOutputStream();
        c.compress(Bitmap.CompressFormat.PNG, 100, cos);
        img_c=cos.toByteArray();

        d= BitmapFactory.decodeResource(getResources(), R.drawable.pizza);
        dos=new ByteArrayOutputStream();
        d.compress(Bitmap.CompressFormat.PNG, 100, dos);
        img_d=dos.toByteArray();

        cv=new ContentValues();
        cv.put("question", "Auto");
        cv.put("right", "img_a");
        cv.put("id", 1);
        cv.put("image_a", img_a);
        cv.put("image_b", img_b);
        cv.put("image_c", img_c);
        cv.put("image_d", img_d);
        db.insert("imageQuestion", null, cv);

        //Brot
        a= BitmapFactory.decodeResource(getResources(), R.drawable.apfel);
        aos=new ByteArrayOutputStream();
        a.compress(Bitmap.CompressFormat.PNG, 100, aos);
        img_a=aos.toByteArray();

        b= BitmapFactory.decodeResource(getResources(), R.drawable.brot);
        bos=new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.PNG, 100, bos);
        img_b=bos.toByteArray();

        c= BitmapFactory.decodeResource(getResources(), R.drawable.auto);
        cos=new ByteArrayOutputStream();
        c.compress(Bitmap.CompressFormat.PNG, 100, cos);
        img_c=cos.toByteArray();

        d= BitmapFactory.decodeResource(getResources(), R.drawable.pizza);
        dos=new ByteArrayOutputStream();
        d.compress(Bitmap.CompressFormat.PNG, 100, dos);
        img_d=dos.toByteArray();

        cv=new ContentValues();
        cv.put("question", "Brot");
        cv.put("right", "img_b");
        cv.put("id", 2);
        cv.put("image_a", img_a);
        cv.put("image_b", img_b);
        cv.put("image_c", img_c);
        cv.put("image_d", img_d);
        db.insert("imageQuestion", null, cv);


        //Zug
        a= BitmapFactory.decodeResource(getResources(), R.drawable.fahrrad);
        aos=new ByteArrayOutputStream();
        a.compress(Bitmap.CompressFormat.PNG, 100, aos);
        img_a=aos.toByteArray();

        b= BitmapFactory.decodeResource(getResources(), R.drawable.auto);
        bos=new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.PNG, 100, bos);
        img_b=bos.toByteArray();

        c= BitmapFactory.decodeResource(getResources(), R.drawable.zug);
        cos=new ByteArrayOutputStream();
        c.compress(Bitmap.CompressFormat.PNG, 100, cos);
        img_c=cos.toByteArray();

        d= BitmapFactory.decodeResource(getResources(), R.drawable.pizza);
        dos=new ByteArrayOutputStream();
        d.compress(Bitmap.CompressFormat.PNG, 100, dos);
        img_d=dos.toByteArray();

        cv=new ContentValues();
        cv.put("question", "Zug");
        cv.put("right", "img_c");
        cv.put("id", 3);
        cv.put("image_a", img_a);
        cv.put("image_b", img_b);
        cv.put("image_c", img_c);
        cv.put("image_d", img_d);
        db.insert("imageQuestion", null, cv);

        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent intent = new Intent(ImageDB.this,BohnenSpiel_ImageMode.class);
        intent.putExtra("level",temp);
        intent.putExtra("bohnen",tempBohnen);
        startActivity(intent);

    }
}
