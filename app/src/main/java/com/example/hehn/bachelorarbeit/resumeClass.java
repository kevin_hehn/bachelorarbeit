package com.example.hehn.bachelorarbeit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.example.hehn.bachelorarbeit.Spiel.Bohnenspiel;

/**
 * Created by Hehn on 03.01.2016.
 */
public class resumeClass extends AppCompatActivity {
    public void terminate(){
        Intent resumeBohnenspiel = new Intent(resumeClass.this,Bohnenspiel.class);
        resumeBohnenspiel.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        //resumeBohnenspiel.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(resumeBohnenspiel);
    }

}
