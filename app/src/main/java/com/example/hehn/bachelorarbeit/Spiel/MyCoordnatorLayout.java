package com.example.hehn.bachelorarbeit.Spiel;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

/**
 * Created by Hehn on 17.01.2016.
 */
public class MyCoordnatorLayout extends CoordinatorLayout{
    public MyCoordnatorLayout(Context context) {
        super(context);
    }

    public MyCoordnatorLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyCoordnatorLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        Log.d("Coordinator",Integer.toString(event.getAction()));
        return false;
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Never allow swiping to switch between pages
        return false;
    }
}
