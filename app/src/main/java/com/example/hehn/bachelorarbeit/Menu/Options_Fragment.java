package com.example.hehn.bachelorarbeit.Menu;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.example.hehn.bachelorarbeit.R;
import com.example.hehn.bachelorarbeit.database.Catalogue_Selection;
import com.example.hehn.bachelorarbeit.database.ImageDB;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Options_Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Options_Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Options_Fragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    SeekBar fade_seek;
    TextView fade_text;
    ToggleButton vibration;
    Button deleteStatistics;
    Button reloadQuestions;
    SharedPreferences sharedpreferences;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Options_Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Options_Fragment newInstance(String param1, String param2) {
        Options_Fragment fragment = new Options_Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public Options_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        FrameLayout FL = (FrameLayout)inflater.inflate(R.layout.fragment_options_, container, false);

        sharedpreferences = this.getActivity().getSharedPreferences("MyPREFERENCES", Context.MODE_PRIVATE);
        fade_seek = (SeekBar) FL.findViewById(R.id.seekBar);
        fade_text = (TextView) FL.findViewById(R.id.textView1);
        deleteStatistics = (Button) FL.findViewById(R.id.deleteStatistics);
        vibration = (ToggleButton) FL.findViewById(R.id.vibration);
        reloadQuestions = (Button) FL.findViewById(R.id.reloadQuestions);

        vibration.setChecked(Boolean.parseBoolean(sharedpreferences.getString("vibration", "false")));
        vibration.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    Log.d("alarmCheck", "ALARM SET TO TRUE");
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.remove("vibration");
                    editor.commit();
                    editor.putString("vibration", Boolean.toString(isChecked));
                    editor.commit();
                } else {
                    Log.d("alarmCheck", "ALARM SET TO FALSE");
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.remove("vibration");
                    editor.commit();
                    editor.putString("vibration", Boolean.toString(isChecked));
                    editor.commit();
                }
            }
        });
        reloadQuestions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), Catalogue_Selection.class);
                startActivity(intent);
            }
        });

        deleteStatistics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder( getActivity() )
                        .setTitle( getString(R.string.options_caution))
                        .setMessage( getString(R.string.options_delete) )
                        .setPositiveButton( "Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                SQLiteDatabase mydatabase = getActivity().openOrCreateDatabase("StatisticDB", getActivity().MODE_PRIVATE, null);
                                mydatabase.execSQL("DROP TABLE Questions");
                                mydatabase.execSQL("DROP TABLE games");
                                mydatabase.execSQL("CREATE TABLE IF NOT EXISTS Questions (right bit, wrong bit)");
                                mydatabase.execSQL("CREATE TABLE IF NOT EXISTS games (level int,date date,won int)");
                            }
                        })
                        .setNegativeButton( getString(R.string.options_cancel), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Log.d( "AlertDialog", "Negative" );
                            }
                        } )
                        .show();
            }
        });



        fade_text.setText(Integer.toString(Integer.parseInt(sharedpreferences.getString("bohnen","2"))));
        fade_seek.setProgress(Integer.parseInt(sharedpreferences.getString("bohnen", "2"))-1);

        fade_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
                fade_text.setText(Integer.toString(progress+1));
                Log.d("onProgressChanged", Integer.toString(progress));

                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.remove("bohnen");
                editor.commit();
                editor.putString("bohnen", Integer.toString(progress+1));
                editor.commit();
            }
        });
        return FL;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    public void onResume(){
        super.onResume();

        // Set title bar
        ((Main_Menu) getActivity()).setActionBarTitle("Options");
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
