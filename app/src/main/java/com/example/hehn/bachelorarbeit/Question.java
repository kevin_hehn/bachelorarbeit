package com.example.hehn.bachelorarbeit;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.GridView;
import android.widget.TextView;

import com.example.hehn.bachelorarbeit.Spiel.Bohnenspiel;
import com.example.hehn.bachelorarbeit.Spiel.ButtonClass;

public class Question extends AppCompatActivity {
    String Question;
    String [] Answers;
    int rightAnswer = 0;
    GridView gridlayout;
    public static boolean answered = false;
    public static boolean right = false;
    SQLiteDatabase mydatabase;
    TextView tw;

    Thread thread = new Thread() {
        @Override
        public void run() {
            while(true) {
                if(answered){
                    Intent resumeBohnenspiel = new Intent(com.example.hehn.bachelorarbeit.Question.this,Bohnenspiel.class);
                    resumeBohnenspiel.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    if(right == true){
                        resumeBohnenspiel.putExtra("points",4);
                        Log.d("onResumeIntent","richtig");
                    }
                    else{
                        resumeBohnenspiel.putExtra("points",0);
                        Log.d("onResumeIntent", "richtig");
                    }
                    startActivity(resumeBohnenspiel);
                    break;
                }
            }
            Log.d("Question", "Kurz bevor Finish");
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tw = (TextView) findViewById(R.id.questionText);
        gridlayout = (GridView) findViewById(R.id.questionGridLayout);
        mydatabase = openOrCreateDatabase("QuestionDB",MODE_PRIVATE,null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        answered = false;
        right = false;

        Log.d("Inside Question", "sd");
        Cursor resultSet = mydatabase.rawQuery("Select max(ID) from Questions", null);
        resultSet.moveToFirst();
        int maxID = resultSet.getInt(0);
        int randomID = (int)(Math.random()*maxID+1);
        resultSet.close();

        resultSet = mydatabase.rawQuery("Select * from Questions WHERE ID = "+randomID+"", null);
        resultSet.moveToFirst();
        Question = resultSet.getString(1);
        Log.d("Anzahl Frage", Question);
        resultSet.close();

        resultSet = mydatabase.rawQuery("Select * from Answers WHERE ID = "+randomID+"", null);
        resultSet.moveToFirst();
        String Answer_1 = resultSet.getString(1);
        String Answer_2 = resultSet.getString(2);
        String Answer_3 = resultSet.getString(3);
        String Answer_4 = resultSet.getString(4);
        rightAnswer = resultSet.getInt(5);

        Answers = new String[4];
        Answers[0] = Answer_1;
        Answers[1] = Answer_2;
        Answers[2] = Answer_3;
        Answers[3] = Answer_4;
        Log.d("Anzahl Frage",Question);


        tw.setText(Question);
        gridlayout.setAdapter(new ButtonClass(getApplicationContext(), Answers.clone(), rightAnswer));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        thread.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}


