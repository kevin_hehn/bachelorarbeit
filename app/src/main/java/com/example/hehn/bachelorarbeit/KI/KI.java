package com.example.hehn.bachelorarbeit.KI;

import android.os.SystemClock;
import android.util.Log;

/**
 * Created by Hehn on 10.01.2016.
 */

public class KI {
    public static int gespeicherterZug = 0;
    public static int gewTiefe = 3;
    public int level;
    public int p1;
    public int p2;
    int[] board_init;
    public static void main(String[] args) {
        int[] board = { 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 3 };
        //int[] board = { 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6 };
        //int[] board = { 0, 0, 0, 6, 6, 6, 0, 0, 6, 6, 6, 6 };
        KI a = new KI(2);
        a.startKI(board.clone());
        System.out.println("*******");
        System.out.println(gespeicherterZug+6);
//		Main.updateBoard(board2, 3);
//    	for(int a=0;a<12;a++){
//    		System.out.print(board2[a]); //Alle verbleibenden Steine
//    	}
//    	//System.out.println("");
//    	for(int a=0;a<12;a++){
//    		System.out.print(board[a]); //Alle verbleibenden Steine
//    	}
    }
    public KI(int level){
        this.level = level;
    }

    public int startKI(int[] gb){
        if(this.level == 0){
            int random = -100;
            while (random < 5 || random > 11 || gb[random] == 0) {
                random = (int) (Math.random() * 6);
                random = random + 6;
                Log.d("KI_RANDOM",Integer.toString(random));
            }
            gespeicherterZug = random;
            gespeicherterZug = gespeicherterZug-6;
        }
        else if (this.level == 1){
            board_init = gb.clone();
            max(2, 3, board_init.clone(), 6);
            if (board_init[gespeicherterZug + 6] == 0) {
                while (board_init[gespeicherterZug + 6] == 0) {
                    gespeicherterZug = (int) (Math.random() * 6);
                    System.out.println(gespeicherterZug);
                }
            }
        }
        else{
            this.gewTiefe = 6;
            board_init = gb.clone();
            max(2, 6, board_init.clone(), 6);
            if (board_init[gespeicherterZug + 6] == 0) {
                while (board_init[gespeicherterZug + 6] == 0) {
                    gespeicherterZug = (int) (Math.random() * 6);
                    System.out.println(gespeicherterZug);
                }
            }
        }
        /*
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        */
        return gespeicherterZug;
    }

    public  int max(int spieler, int tiefe, int[] brd, int offset ) {
		System.out.println("Spieler"+spieler+" Tiefer:"+tiefe);
        int sum = 0;
        int sum_old  = 0;
        int sum_ges = 0;
        if (tiefe == 0)// or keineZuegeMehr(spieler))
            return 0; //bewerten();
        int maxWert = -10000;

        int [] board2 = new int [12];
        board2 = (int[]) brd.clone();
        for (int i = 0; i<6;i++){
            if(board2[i+offset] == 0)
                continue;
            brd =  (int[]) updateBoard(board2, i+offset).clone();
            System.out.println(i);
            int wert = min(-spieler,tiefe-1,(int[])brd.clone());
            System.out.println("IN MAX: "+wert);
            for(int a=0;a<12;a++){
                sum_old += board2[a]; //Alle verbleibenden Steine
                sum += brd[a];
            }
            System.out.println(sum_old-sum);
//		    	for(int a=0;a<12;a++){
//		    		System.out.print(board2[a]); //Alle verbleibenden Steine
//		    	}
//		    	//System.out.println("");
//		    	for(int a=0;a<12;a++){
//		    		System.out.print(brd[a]); //Alle verbleibenden Steine
//		    	}
//		    	//System.out.println("");
            wert = wert +(sum_old - sum);
//		    	//System.out.println(wert);
            if(wert > maxWert){
                maxWert = wert;
                sum_ges = sum_old - sum;
                if(tiefe == KI.gewTiefe){ //gewünschte Tiefe = 6
                    KI.gespeicherterZug = i;
                }
            }
            sum =0;
            sum_old=0;
        }
//		    //System.out.println("Spieler:"+spieler+" Tiefe:"+tiefe+"Wert:"+(maxWert+sum_ges));
//		    System.out.println(maxWert);
        return (maxWert);
    }

    public  int min(int spieler, int tiefe, int[] board) {
		System.out.println("Spieler"+spieler+" Tiefer:"+tiefe);
        int sum = 0;
        int sum_old  = 0;
        int sum_ges = 0;
        if (tiefe == 0)// or keineZuegeMehr(spieler))
            return 0;//bewerten();
        int minWert = 10000000;
        int [] board2 = (int[]) board.clone();
        for (int i = 0; i<6;i++){
            if(board2[i] == 0)
                continue;
            board = (int[]) updateBoard(board2, i).clone();
            System.out.println("Tiefe "+tiefe);
            int wert = max(-spieler,tiefe-1,(int[])board.clone(),6);
            System.out.println("In MIN: "+wert+"  "+i);
            for(int a=0;a<12;a++){
                sum_old += board2[a]; //Alle verbleibenden Steine
                sum += board[a];

            }


            wert = wert -(sum_old - sum);
            if(wert < minWert){
                minWert = wert;
                sum_ges = (sum_old-sum);
                //System.out.println("");
            }
            sum =0;
            sum_old=0;
        }
        //System.out.println("Spieler:"+spieler+" Tiefe:"+tiefe+"Wert:"+(minWert-sum_ges)+"SUM_GES"+sum_ges);
//		    System.out.println(minWert);
        return (minWert);
    }
    public int[] updateBoard(int[] b, int field) {
        int[] board = (int[]) b.clone();
        int startField = field;

        int value = board[field];
        board[field] = 0;
        while (value > 0) {
            field = (++field) % 12;
            board[field]++;
            value--;
        }

        if ((board[field] == 2) || (board[field] == 4) || (board[field] == 6)) {
            do {
                if (startField < 6) {
                    p1 += board[field];
                } else {
                    p2 += board[field];
                }
                board[field] = 0;
                field = (field == 0) ? field = 11 : --field;
            } while ((board[field] == 2) || (board[field] == 4) || (board[field] == 6));
        }
        return board;
    }
}