package com.example.hehn.bachelorarbeit.Spiel;


import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.DragEvent;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.hehn.bachelorarbeit.KI.KI;
import com.example.hehn.bachelorarbeit.R;
import com.example.hehn.bachelorarbeit.database.database;

public class Bohnenspiel_Mario extends AppCompatActivity implements View.OnTouchListener{

    public int[] gameboard;
    public int points_A;
    public int points_B;
    Button button_0;
    Button button_1;
    Button button_2;
    Button button_3;
    Button button_4;
    Button button_5;
    Button button_6;
    Button button_7;
    Button button_8;
    Button button_9;
    Button button_10;
    Button button_11;
    Button button_12;
    Button result_A;
    Button result_B;
    int lastClicked;
    int selectedDraw;
    boolean buttonsClickable = false;
    Drawable buttonBackground;
    int isKI;
    boolean drawfinished=false;
    TextView answer_1;
    TextView answer_2;
    TextView answer_3;
    TextView answer_4;
    TextView solution;
    ViewGroup _root;
    private int _xDelta;
    private int _yDelta;
    boolean player_A_made_points=false;
    ImageView img;
    TextView _cur;
    boolean isDragged = false;
    boolean pauseBackground = true;
    String selectedAnswer="";
    String rightAnswer="A: Du bist dumm";
    String question = "Was bist du?";
    boolean isAnswerRight = false;
    TextView toolbar;
    database db;
    SQLiteDatabase mydatabase;
    Bundle IntentBundle;
    int KILevel;
    static boolean buttonBlinkStop = false;
    int[] mario;
    ImageView schriftrolle_imageView;

    android.widget.RelativeLayout.LayoutParams layoutParams;
    String msg = "DRAG";


    Thread GameThread = new Thread() {
        @Override
        public void run() {

            boolean player_A_out_of_beans = false;
            boolean player_B_out_of_beans = false;

            while(true) {
                updateButton(true);
                //Wait for Input
                while (buttonsClickable ==true){
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                makeDraw draw = new makeDraw();
                draw.execute(selectedDraw, 0);
                while(drawfinished == false){
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                drawfinished = false;
                if(gameboard[6]==0 &&gameboard[7]==0 &&gameboard[8]==0 &&gameboard[9]==0 &&gameboard[10]==0 &&gameboard[11]==0){
                    player_B_out_of_beans = true;
                    points_A += gameboard[0]+gameboard[1]+gameboard[2]+gameboard[3]+gameboard[4]+gameboard[5];
                    break;
                }
                //******KI Part
                int random = -100;
                /*
                while (random < 5 || random > 11 || gameboard[random] == 0) {
                    random = (int) (Math.random() * 6);
                    random = random + 6;
                    Log.d("Status", "KI" + Integer.toString(random));
                }*/
                try {
                    KILevel = IntentBundle.getInt("level");
                }catch (NullPointerException e){
                    KILevel = 1;
                }
                KI ki = new KI(KILevel);
                random = ki.startKI(gameboard.clone())+6;
                Log.d("KI", Integer.toString(random));
                draw = new makeDraw();
                draw.execute(random, 1);
                while(drawfinished == false){
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                drawfinished = false;
                if(gameboard[0]==0 &&gameboard[1]==0 &&gameboard[2]==0 &&gameboard[3]==0 &&gameboard[4]==0 &&gameboard[5]==0){
                    player_A_out_of_beans = true;
                    points_B += gameboard[6]+gameboard[7]+gameboard[8]+gameboard[9]+gameboard[10]+gameboard[11];
                    break;
                }
                //************
                Log.d("Status:", "User Input available");
            }
            Log.d("Status:", "Spielende");

            for (int i=0;i<12;i++){
                gameboard[i] = 0;
            }
            updateViewsGameThread gt = new updateViewsGameThread();
            gt.execute();
            while(drawfinished == false){
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            drawfinished = false;
            endCard stCheck = new endCard();
            stCheck.execute();
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bohnenspiel);

        schriftrolle_imageView = (ImageView) findViewById(R.id.imageView_Schriftrolle);
        _root = (ViewGroup)findViewById(R.id.root);
        answer_1 = new TextView(this);
        answer_1.setText("Antwort 1");
        answer_2 = new TextView(this);
        answer_2.setText("Antwort 2");
        answer_3 = new TextView(this);
        answer_3.setText("Antwort 3");
        answer_4 = new TextView(this);
        answer_4.setText("Antwort 4");

        toolbar = (TextView) findViewById(R.id.questionTextview);
        toolbar.setText("");

        mario = new int[3];

        //database_connection db = new database_connection (this);
        //db.open();
        //List<Question> values = db.getQuestion();
        //Log.d("DATENBANK",values.get(0));
        //Log.d("DATENBANK",values.get(1));

        button_0 = (Button) findViewById(R.id.button_7);
        button_1 = (Button) findViewById(R.id.button_8);
        button_2 = (Button) findViewById(R.id.button_9);
        button_3 = (Button) findViewById(R.id.button_10);
        button_4 = (Button) findViewById(R.id.button_11);
        button_5 = (Button) findViewById(R.id.button_12);
        button_6 = (Button) findViewById(R.id.button_6);
        button_7 = (Button) findViewById(R.id.button_5);
        button_8 = (Button) findViewById(R.id.button_4);
        button_9 = (Button) findViewById(R.id.button_3);
        button_10 = (Button) findViewById(R.id.button_2);
        button_11 = (Button) findViewById(R.id.button_1);
        result_A = (Button) findViewById(R.id.button_points_a);
        result_B = (Button) findViewById(R.id.button_points_b);


        buttonBackground = button_0.getBackground();

        _root.setOnDragListener(new MyDragListener_2());
        _root.setClipChildren(false);

        result_A.setOnDragListener(new MyDragListener_2());

        gameboard = new int[12];

        IntentBundle = getIntent().getExtras();
        int bohnenAnz = Integer.parseInt(IntentBundle.getString("bohnen"));

        for(int i =0;i<12;i++){
            gameboard[i] = bohnenAnz;
        }
        points_A = 0;
        points_B = 0;

        mydatabase = openOrCreateDatabase("QuestionDB",MODE_PRIVATE,null);
        drawGameboard();

        GameThread.start();

    }
    public void drawGameboard(){
        button_0.setText(Integer.toString(gameboard[0]));
        button_1.setText(Integer.toString(gameboard[1]));
        button_2.setText(Integer.toString(gameboard[2]));
        button_3.setText(Integer.toString(gameboard[3]));
        button_4.setText(Integer.toString(gameboard[4]));
        button_5.setText(Integer.toString(gameboard[5]));
        button_6.setText(Integer.toString(gameboard[6]));
        button_7.setText(Integer.toString(gameboard[7]));
        button_8.setText(Integer.toString(gameboard[8]));
        button_9.setText(Integer.toString(gameboard[9]));
        button_10.setText(Integer.toString(gameboard[10]));
        button_11.setText(Integer.toString(gameboard[11]));
        result_A.setText(Integer.toString(points_A));
        result_B.setText(Integer.toString(points_B));

        switch(gameboard[0]){
            case 0: button_0.setBackgroundResource(R.drawable.mulde); break;
            case 1: button_0.setBackgroundResource(R.drawable.bohnenmulde_1);break;
            case 2: button_0.setBackgroundResource(R.drawable.bohnenmulde_2);break;
            case 3: button_0.setBackgroundResource(R.drawable.bohnenmulde_3);break;
            case 4: button_0.setBackgroundResource(R.drawable.bohnenmulde_4);break;
            case 5: button_0.setBackgroundResource(R.drawable.bohnenmulde_5);break;
            case 6: button_0.setBackgroundResource(R.drawable.bohnenmulde_6);break;
            case 7: button_0.setBackgroundResource(R.drawable.bohnenmulde_7);break;
            case 8: button_0.setBackgroundResource(R.drawable.bohnenmulde_8);break;
            default: button_0.setBackgroundResource(R.drawable.mulde); break;
        }
        switch(gameboard[1]){
            case 0: button_1.setBackgroundResource(R.drawable.mulde); break;
            case 1: button_1.setBackgroundResource(R.drawable.bohnenmulde_1);break;
            case 2: button_1.setBackgroundResource(R.drawable.bohnenmulde_2);break;
            case 3: button_1.setBackgroundResource(R.drawable.bohnenmulde_3);break;
            case 4: button_1.setBackgroundResource(R.drawable.bohnenmulde_4);break;
            case 5: button_1.setBackgroundResource(R.drawable.bohnenmulde_5);break;
            case 6: button_1.setBackgroundResource(R.drawable.bohnenmulde_6);break;
            case 7: button_1.setBackgroundResource(R.drawable.bohnenmulde_7);break;
            case 8: button_1.setBackgroundResource(R.drawable.bohnenmulde_8);break;
            default: button_1.setBackgroundResource(R.drawable.mulde); break;
        }
        switch(gameboard[2]){
            case 0: button_2.setBackgroundResource(R.drawable.mulde); break;
            case 1: button_2.setBackgroundResource(R.drawable.bohnenmulde_1);break;
            case 2: button_2.setBackgroundResource(R.drawable.bohnenmulde_2);break;
            case 3: button_2.setBackgroundResource(R.drawable.bohnenmulde_3);break;
            case 4: button_2.setBackgroundResource(R.drawable.bohnenmulde_4);break;
            case 5: button_2.setBackgroundResource(R.drawable.bohnenmulde_5);break;
            case 6: button_2.setBackgroundResource(R.drawable.bohnenmulde_6);break;
            case 7: button_2.setBackgroundResource(R.drawable.bohnenmulde_7);break;
            case 8: button_2.setBackgroundResource(R.drawable.bohnenmulde_8);break;
            default: button_2.setBackgroundResource(R.drawable.mulde); break;
        }
        switch(gameboard[3]){
            case 0: button_3.setBackgroundResource(R.drawable.mulde); break;
            case 1: button_3.setBackgroundResource(R.drawable.bohnenmulde_1);break;
            case 2: button_3.setBackgroundResource(R.drawable.bohnenmulde_2);break;
            case 3: button_3.setBackgroundResource(R.drawable.bohnenmulde_3);break;
            case 4: button_3.setBackgroundResource(R.drawable.bohnenmulde_4);break;
            case 5: button_3.setBackgroundResource(R.drawable.bohnenmulde_5);break;
            case 6: button_3.setBackgroundResource(R.drawable.bohnenmulde_6);break;
            case 7: button_3.setBackgroundResource(R.drawable.bohnenmulde_7);break;
            case 8: button_3.setBackgroundResource(R.drawable.bohnenmulde_8);break;
            default: button_3.setBackgroundResource(R.drawable.mulde); break;
        }
        switch(gameboard[4]){
            case 0: button_4.setBackgroundResource(R.drawable.mulde); break;
            case 1: button_4.setBackgroundResource(R.drawable.bohnenmulde_1);break;
            case 2: button_4.setBackgroundResource(R.drawable.bohnenmulde_2);break;
            case 3: button_4.setBackgroundResource(R.drawable.bohnenmulde_3);break;
            case 4: button_4.setBackgroundResource(R.drawable.bohnenmulde_4);break;
            case 5: button_4.setBackgroundResource(R.drawable.bohnenmulde_5);break;
            case 6: button_4.setBackgroundResource(R.drawable.bohnenmulde_6);break;
            case 7: button_4.setBackgroundResource(R.drawable.bohnenmulde_7);break;
            case 8: button_4.setBackgroundResource(R.drawable.bohnenmulde_8);break;
            default: button_4.setBackgroundResource(R.drawable.mulde); break;
        }
        switch(gameboard[5]){
            case 0: button_5.setBackgroundResource(R.drawable.mulde); break;
            case 1: button_5.setBackgroundResource(R.drawable.bohnenmulde_1);break;
            case 2: button_5.setBackgroundResource(R.drawable.bohnenmulde_2);break;
            case 3: button_5.setBackgroundResource(R.drawable.bohnenmulde_3);break;
            case 4: button_5.setBackgroundResource(R.drawable.bohnenmulde_4);break;
            case 5: button_5.setBackgroundResource(R.drawable.bohnenmulde_5);break;
            case 6: button_5.setBackgroundResource(R.drawable.bohnenmulde_6);break;
            case 7: button_5.setBackgroundResource(R.drawable.bohnenmulde_7);break;
            case 8: button_5.setBackgroundResource(R.drawable.bohnenmulde_8);break;
            default: button_5.setBackgroundResource(R.drawable.mulde); break;
        }
        switch(gameboard[6]){
            case 0: button_6.setBackgroundResource(R.drawable.mulde); break;
            case 1: button_6.setBackgroundResource(R.drawable.bohnenmulde_1);break;
            case 2: button_6.setBackgroundResource(R.drawable.bohnenmulde_2);break;
            case 3: button_6.setBackgroundResource(R.drawable.bohnenmulde_3);break;
            case 4: button_6.setBackgroundResource(R.drawable.bohnenmulde_4);break;
            case 5: button_6.setBackgroundResource(R.drawable.bohnenmulde_5);break;
            case 6: button_6.setBackgroundResource(R.drawable.bohnenmulde_6);break;
            case 7: button_6.setBackgroundResource(R.drawable.bohnenmulde_7);break;
            case 8: button_6.setBackgroundResource(R.drawable.bohnenmulde_8);break;
            default: button_6.setBackgroundResource(R.drawable.mulde); break;
        }
        switch(gameboard[7]){
            case 0: button_7.setBackgroundResource(R.drawable.mulde); break;
            case 1: button_7.setBackgroundResource(R.drawable.bohnenmulde_1);break;
            case 2: button_7.setBackgroundResource(R.drawable.bohnenmulde_2);break;
            case 3: button_7.setBackgroundResource(R.drawable.bohnenmulde_3);break;
            case 4: button_7.setBackgroundResource(R.drawable.bohnenmulde_4);break;
            case 5: button_7.setBackgroundResource(R.drawable.bohnenmulde_5);break;
            case 6: button_7.setBackgroundResource(R.drawable.bohnenmulde_6);break;
            case 7: button_7.setBackgroundResource(R.drawable.bohnenmulde_7);break;
            case 8: button_7.setBackgroundResource(R.drawable.bohnenmulde_8);break;
            default: button_7.setBackgroundResource(R.drawable.mulde); break;
        }
        switch(gameboard[8]){
            case 0: button_8.setBackgroundResource(R.drawable.mulde); break;
            case 1: button_8.setBackgroundResource(R.drawable.bohnenmulde_1);break;
            case 2: button_8.setBackgroundResource(R.drawable.bohnenmulde_2);break;
            case 3: button_8.setBackgroundResource(R.drawable.bohnenmulde_3);break;
            case 4: button_8.setBackgroundResource(R.drawable.bohnenmulde_4);break;
            case 5: button_8.setBackgroundResource(R.drawable.bohnenmulde_5);break;
            case 6: button_8.setBackgroundResource(R.drawable.bohnenmulde_6);break;
            case 7: button_8.setBackgroundResource(R.drawable.bohnenmulde_7);break;
            case 8: button_8.setBackgroundResource(R.drawable.bohnenmulde_8);break;
            default: button_8.setBackgroundResource(R.drawable.mulde); break;
        }
        switch(gameboard[9]){
            case 0: button_9.setBackgroundResource(R.drawable.mulde); break;
            case 1: button_9.setBackgroundResource(R.drawable.bohnenmulde_1);break;
            case 2: button_9.setBackgroundResource(R.drawable.bohnenmulde_2);break;
            case 3: button_9.setBackgroundResource(R.drawable.bohnenmulde_3);break;
            case 4: button_9.setBackgroundResource(R.drawable.bohnenmulde_4);break;
            case 5: button_9.setBackgroundResource(R.drawable.bohnenmulde_5);break;
            case 6: button_9.setBackgroundResource(R.drawable.bohnenmulde_6);break;
            case 7: button_9.setBackgroundResource(R.drawable.bohnenmulde_7);break;
            case 8: button_9.setBackgroundResource(R.drawable.bohnenmulde_8);break;
            default: button_9.setBackgroundResource(R.drawable.mulde); break;
        }
        switch(gameboard[10]){
            case 0: button_10.setBackgroundResource(R.drawable.mulde); break;
            case 1: button_10.setBackgroundResource(R.drawable.bohnenmulde_1);break;
            case 2: button_10.setBackgroundResource(R.drawable.bohnenmulde_2);break;
            case 3: button_10.setBackgroundResource(R.drawable.bohnenmulde_3);break;
            case 4: button_10.setBackgroundResource(R.drawable.bohnenmulde_4);break;
            case 5: button_10.setBackgroundResource(R.drawable.bohnenmulde_5);break;
            case 6: button_10.setBackgroundResource(R.drawable.bohnenmulde_6);break;
            case 7: button_10.setBackgroundResource(R.drawable.bohnenmulde_7);break;
            case 8: button_10.setBackgroundResource(R.drawable.bohnenmulde_8);break;
            default: button_10.setBackgroundResource(R.drawable.mulde); break;
        }
        switch(gameboard[11]){
            case 0: button_11.setBackgroundResource(R.drawable.mulde); break;
            case 1: button_11.setBackgroundResource(R.drawable.bohnenmulde_1);break;
            case 2: button_11.setBackgroundResource(R.drawable.bohnenmulde_2);break;
            case 3: button_11.setBackgroundResource(R.drawable.bohnenmulde_3);break;
            case 4: button_11.setBackgroundResource(R.drawable.bohnenmulde_4);break;
            case 5: button_11.setBackgroundResource(R.drawable.bohnenmulde_5);break;
            case 6: button_11.setBackgroundResource(R.drawable.bohnenmulde_6);break;
            case 7: button_11.setBackgroundResource(R.drawable.bohnenmulde_7);break;
            case 8: button_11.setBackgroundResource(R.drawable.bohnenmulde_8);break;
            default: button_11.setBackgroundResource(R.drawable.mulde); break;
        }
        int rand;
        rand = (int) (Math.random()*12);
        mario[0] = rand;
        switch (rand){
            case 0:button_0.setBackgroundResource(R.drawable.mario);break;
            case 1:button_1.setBackgroundResource(R.drawable.mario);break;
            case 2:button_2.setBackgroundResource(R.drawable.mario);break;
            case 3:button_3.setBackgroundResource(R.drawable.mario);break;
            case 4:button_4.setBackgroundResource(R.drawable.mario);break;
            case 5:button_5.setBackgroundResource(R.drawable.mario);break;
            case 6:button_6.setBackgroundResource(R.drawable.mario);break;
            case 7:button_7.setBackgroundResource(R.drawable.mario);break;
            case 8:button_8.setBackgroundResource(R.drawable.mario);break;
            case 9:button_9.setBackgroundResource(R.drawable.mario);break;
            case 10:button_10.setBackgroundResource(R.drawable.mario);break;
            case 11:button_11.setBackgroundResource(R.drawable.mario);break;
        }
    }
    public void updateButton(boolean isClickable){
        buttonsClickable = isClickable;
        if(isClickable == true){
            button_0.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if(gameboard[0]>0)
                        isClicked(0);
                }
            });
            button_1.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if(gameboard[1]>0)
                        isClicked(1);
                }
            });
            button_2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if(gameboard[2]>0)
                        isClicked(2);
                }
            });
            button_3.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if(gameboard[3]>0)
                        isClicked(3);
                }
            });
            button_4.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if(gameboard[4]>0)
                        isClicked(4);
                }
            });
            button_5.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if(gameboard[5]>0)
                        isClicked(5);
                }
            });
        }
        if (isClickable == false) {
            button_0.setOnClickListener(null);
            button_1.setOnClickListener(null);
            button_2.setOnClickListener(null);
            button_3.setOnClickListener(null);
            button_4.setOnClickListener(null);
            button_5.setOnClickListener(null);
        }
        button_0.setClickable(isClickable);
        button_1.setClickable(isClickable);
        button_2.setClickable(isClickable);
        button_3.setClickable(isClickable);
        button_4.setClickable(isClickable);
        button_5.setClickable(isClickable);
        button_6.setClickable(isClickable);
        button_7.setClickable(isClickable);
        button_8.setClickable(isClickable);
        button_9.setClickable(isClickable);
        button_10.setClickable(isClickable);
        button_11.setClickable(isClickable);
    }

    public void isClicked(int ID){
        if(buttonsClickable == true){
            updateButton(false);
            selectedDraw = ID;
            Log.d("Ausgewählter Zug",Integer.toString(selectedDraw));
        }



    }
    public void makeDraw(int ID){

    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        final int X = (int) event.getRawX();
        final int Y = (int) event.getRawY();
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                _xDelta = X - lParams.leftMargin;
                _yDelta = Y - lParams.topMargin;
                break;
            case MotionEvent.ACTION_UP:

                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                break;
            case MotionEvent.ACTION_POINTER_UP:
                break;
            case MotionEvent.ACTION_MOVE:
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                layoutParams.leftMargin = X - _xDelta;
                layoutParams.topMargin = Y - _yDelta;
                layoutParams.rightMargin = -250;
                layoutParams.bottomMargin = -250;
                view.setLayoutParams(layoutParams);
                break;
        }
        _root.invalidate();
        return true;
    }

    private class makeDraw extends AsyncTask<Integer, Integer, Integer> {

        protected Integer doInBackground(Integer... pos) {
            //Test ob ein Spieler keine Steine mehr zur Verfügung hat.
            int position = pos[0];


            //Sind noch genug Steine vorhanden, so kann der Zug ausgeführt werden.

            isKI = pos[1];
            int toDistribute;
            toDistribute = gameboard[position];
            gameboard[position] = 0;
            publishProgress(position,0);
            publishProgress(1000,0);
            for(;toDistribute>0; toDistribute--){
                int temp;
                int temp_value_button;
                position = position + 1;
                if(position > 11){
                    position = 0;
                }
                temp = gameboard[position];
                gameboard[position] = temp+1;
                publishProgress(position,temp+1);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //Change Button Color back
                publishProgress(1000,0);
            }
            //Berechnung der erreichten Punkte
            while(1==1){
                if(gameboard[position]==2 || gameboard[position]==4 || gameboard[position]== 6) {
                    int rand=mario[0];

                    int temp = gameboard[position];
                    if(position == mario[0]){
                        temp = temp*2;
                        rand = (int) (Math.random()*12);
                    }
                    gameboard[position] = 0;
                    publishProgress(position, 0,1);
                    mario[0] = rand;
                    //Player Human gets the points
                    if (isKI  == 0) {
                        //*****************
                        publishProgress(2000,0);
                        while(pauseBackground){
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        pauseBackground = true;
                        if(!isAnswerRight) {
                            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                            // Vibrate for 500 milliseconds
                            v.vibrate(500);

                            SQLiteDatabase mydatabase = openOrCreateDatabase("StatisticDB",MODE_PRIVATE,null);
                            mydatabase.execSQL("INSERT INTO Questions VALUES (0,1)");
                            publishProgress(3001, 0);
                            try {
                                Thread.sleep(2000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }

                        publishProgress(3000, 0);

                        //******************
                        if(isAnswerRight) {
                            SQLiteDatabase mydatabase = openOrCreateDatabase("StatisticDB",MODE_PRIVATE,null);
                            mydatabase.execSQL("INSERT INTO Questions VALUES (1,0)");
                            points_A = points_A + temp;
                        }
                        publishProgress(40, points_A);
                        if(position==0){
                            position=11;
                        }
                        else {
                            position--;
                        }
                    }
                    if (isKI  == 1) {
                        points_B = points_B + temp;
                        publishProgress(50, points_B);
                        if(position==0){
                            position=11;
                        }
                        else {
                            position--;
                        }
                    }
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    publishProgress(1000, 0);
                    //Mario Neu:
                }
                else{
                    break;
                }

            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            drawfinished = true;
            return null;
        }

        /***********************************************
         * Hier wird die UI geupdatet.
         * @param progress
         */
        protected void onProgressUpdate(Integer... progress) {
            int position = progress[0];
            int value = progress[1];
            int isGreen = 0;
            if(progress.length == 3) {
                try {
                    isGreen = progress[2];
                } catch (Exception e) {
                    Log.d("Error", "Exception abgefangen");
                }
            }

            //Position = 1000 => Zurücksetzen der Felder auf Ausgangszustand.
            //Position 0-11   => Das jeweilige Feld wurde ausgelöst
            //Position 40     => A bekommt die Punkte
            //Position 50     => B bekommt die Punkte
            if(position ==1000){
                if(mario[0]==0){
                    switch(gameboard[0]){
                        case 0: button_0.setBackgroundResource(R.drawable.mario); break;
                        case 1: button_0.setBackgroundResource(R.drawable.mario);break;
                        case 2: button_0.setBackgroundResource(R.drawable.mario);break;
                        case 3: button_0.setBackgroundResource(R.drawable.mario);break;
                        case 4: button_0.setBackgroundResource(R.drawable.mario);break;
                        case 5: button_0.setBackgroundResource(R.drawable.mario);break;
                        case 6: button_0.setBackgroundResource(R.drawable.mario);break;
                        case 7: button_0.setBackgroundResource(R.drawable.mario);break;
                        case 8: button_0.setBackgroundResource(R.drawable.mario);break;
                        default: button_0.setBackgroundResource(R.drawable.mario); break;
                    }
                }else{
                    switch(gameboard[0]){
                        case 0: button_0.setBackgroundResource(R.drawable.mulde); break;
                        case 1: button_0.setBackgroundResource(R.drawable.bohnenmulde_1);break;
                        case 2: button_0.setBackgroundResource(R.drawable.bohnenmulde_2);break;
                        case 3: button_0.setBackgroundResource(R.drawable.bohnenmulde_3);break;
                        case 4: button_0.setBackgroundResource(R.drawable.bohnenmulde_4);break;
                        case 5: button_0.setBackgroundResource(R.drawable.bohnenmulde_5);break;
                        case 6: button_0.setBackgroundResource(R.drawable.bohnenmulde_6);break;
                        case 7: button_0.setBackgroundResource(R.drawable.bohnenmulde_7);break;
                        case 8: button_0.setBackgroundResource(R.drawable.bohnenmulde_8);break;
                        default: button_0.setBackgroundResource(R.drawable.mulde); break;
                    }
                }
                if(mario[0]==1){
                    switch(gameboard[1]){
                        case 0: button_1.setBackgroundResource(R.drawable.mario); break;
                        case 1: button_1.setBackgroundResource(R.drawable.mario);break;
                        case 2: button_1.setBackgroundResource(R.drawable.mario);break;
                        case 3: button_1.setBackgroundResource(R.drawable.mario);break;
                        case 4: button_1.setBackgroundResource(R.drawable.mario);break;
                        case 5: button_1.setBackgroundResource(R.drawable.mario);break;
                        case 6: button_1.setBackgroundResource(R.drawable.mario);break;
                        case 7: button_1.setBackgroundResource(R.drawable.mario);break;
                        case 8: button_1.setBackgroundResource(R.drawable.mario);break;
                        default: button_1.setBackgroundResource(R.drawable.mario); break;
                    }
                }else {
                    switch (gameboard[1]) {
                        case 0:
                            button_1.setBackgroundResource(R.drawable.mulde);
                            break;
                        case 1:
                            button_1.setBackgroundResource(R.drawable.bohnenmulde_1);
                            break;
                        case 2:
                            button_1.setBackgroundResource(R.drawable.bohnenmulde_2);
                            break;
                        case 3:
                            button_1.setBackgroundResource(R.drawable.bohnenmulde_3);
                            break;
                        case 4:
                            button_1.setBackgroundResource(R.drawable.bohnenmulde_4);
                            break;
                        case 5:
                            button_1.setBackgroundResource(R.drawable.bohnenmulde_5);
                            break;
                        case 6:
                            button_1.setBackgroundResource(R.drawable.bohnenmulde_6);
                            break;
                        case 7:
                            button_1.setBackgroundResource(R.drawable.bohnenmulde_7);
                            break;
                        case 8:
                            button_1.setBackgroundResource(R.drawable.bohnenmulde_8);
                            break;
                        default: button_1.setBackgroundResource(R.drawable.mulde); break;
                    }
                }
                if(mario[0]==2){
                    switch(gameboard[2]){
                        case 0: button_2.setBackgroundResource(R.drawable.mario); break;
                        case 1: button_2.setBackgroundResource(R.drawable.mario);break;
                        case 2: button_2.setBackgroundResource(R.drawable.mario);break;
                        case 3: button_2.setBackgroundResource(R.drawable.mario);break;
                        case 4: button_2.setBackgroundResource(R.drawable.mario);break;
                        case 5: button_2.setBackgroundResource(R.drawable.mario);break;
                        case 6: button_2.setBackgroundResource(R.drawable.mario);break;
                        case 7: button_2.setBackgroundResource(R.drawable.mario);break;
                        case 8: button_2.setBackgroundResource(R.drawable.mario);break;
                        default: button_2.setBackgroundResource(R.drawable.mario); break;
                    }
                }else {
                    switch (gameboard[2]) {
                        case 0:
                            button_2.setBackgroundResource(R.drawable.mulde);
                            break;
                        case 1:
                            button_2.setBackgroundResource(R.drawable.bohnenmulde_1);
                            break;
                        case 2:
                            button_2.setBackgroundResource(R.drawable.bohnenmulde_2);
                            break;
                        case 3:
                            button_2.setBackgroundResource(R.drawable.bohnenmulde_3);
                            break;
                        case 4:
                            button_2.setBackgroundResource(R.drawable.bohnenmulde_4);
                            break;
                        case 5:
                            button_2.setBackgroundResource(R.drawable.bohnenmulde_5);
                            break;
                        case 6:
                            button_2.setBackgroundResource(R.drawable.bohnenmulde_6);
                            break;
                        case 7:
                            button_2.setBackgroundResource(R.drawable.bohnenmulde_7);
                            break;
                        case 8:
                            button_2.setBackgroundResource(R.drawable.bohnenmulde_8);
                            break;
                        default: button_2.setBackgroundResource(R.drawable.mulde); break;
                    }
                }
                if(mario[0]==3){
                    switch(gameboard[3]){
                        case 0: button_3.setBackgroundResource(R.drawable.mario); break;
                        case 1: button_3.setBackgroundResource(R.drawable.mario);break;
                        case 2: button_3.setBackgroundResource(R.drawable.mario);break;
                        case 3: button_3.setBackgroundResource(R.drawable.mario);break;
                        case 4: button_3.setBackgroundResource(R.drawable.mario);break;
                        case 5: button_3.setBackgroundResource(R.drawable.mario);break;
                        case 6: button_3.setBackgroundResource(R.drawable.mario);break;
                        case 7: button_3.setBackgroundResource(R.drawable.mario);break;
                        case 8: button_3.setBackgroundResource(R.drawable.mario);break;
                        default: button_3.setBackgroundResource(R.drawable.mario); break;
                    }
                }else{
                    switch(gameboard[3]){
                        case 0: button_3.setBackgroundResource(R.drawable.mulde); break;
                        case 1: button_3.setBackgroundResource(R.drawable.bohnenmulde_1);break;
                        case 2: button_3.setBackgroundResource(R.drawable.bohnenmulde_2);break;
                        case 3: button_3.setBackgroundResource(R.drawable.bohnenmulde_3);break;
                        case 4: button_3.setBackgroundResource(R.drawable.bohnenmulde_4);break;
                        case 5: button_3.setBackgroundResource(R.drawable.bohnenmulde_5);break;
                        case 6: button_3.setBackgroundResource(R.drawable.bohnenmulde_6);break;
                        case 7: button_3.setBackgroundResource(R.drawable.bohnenmulde_7);break;
                        case 8: button_3.setBackgroundResource(R.drawable.bohnenmulde_8);break;
                        default: button_3.setBackgroundResource(R.drawable.mulde); break;
                    }
                }
                if(mario[0]==4){
                    switch(gameboard[4]){
                        case 0: button_4.setBackgroundResource(R.drawable.mario); break;
                        case 1: button_4.setBackgroundResource(R.drawable.mario);break;
                        case 2: button_4.setBackgroundResource(R.drawable.mario);break;
                        case 3: button_4.setBackgroundResource(R.drawable.mario);break;
                        case 4: button_4.setBackgroundResource(R.drawable.mario);break;
                        case 5: button_4.setBackgroundResource(R.drawable.mario);break;
                        case 6: button_4.setBackgroundResource(R.drawable.mario);break;
                        case 7: button_4.setBackgroundResource(R.drawable.mario);break;
                        case 8: button_4.setBackgroundResource(R.drawable.mario);break;
                        default: button_4.setBackgroundResource(R.drawable.mario); break;
                    }
                }else{
                    switch(gameboard[4]){
                        case 0: button_4.setBackgroundResource(R.drawable.mulde); break;
                        case 1: button_4.setBackgroundResource(R.drawable.bohnenmulde_1);break;
                        case 2: button_4.setBackgroundResource(R.drawable.bohnenmulde_2);break;
                        case 3: button_4.setBackgroundResource(R.drawable.bohnenmulde_3);break;
                        case 4: button_4.setBackgroundResource(R.drawable.bohnenmulde_4);break;
                        case 5: button_4.setBackgroundResource(R.drawable.bohnenmulde_5);break;
                        case 6: button_4.setBackgroundResource(R.drawable.bohnenmulde_6);break;
                        case 7: button_4.setBackgroundResource(R.drawable.bohnenmulde_7);break;
                        case 8: button_4.setBackgroundResource(R.drawable.bohnenmulde_8);break;
                        default: button_4.setBackgroundResource(R.drawable.mulde); break;
                    }
                }
                if(mario[0]==5){
                    switch(gameboard[5]){
                        case 0: button_5.setBackgroundResource(R.drawable.mario); break;
                        case 1: button_5.setBackgroundResource(R.drawable.mario);break;
                        case 2: button_5.setBackgroundResource(R.drawable.mario);break;
                        case 3: button_5.setBackgroundResource(R.drawable.mario);break;
                        case 4: button_5.setBackgroundResource(R.drawable.mario);break;
                        case 5: button_5.setBackgroundResource(R.drawable.mario);break;
                        case 6: button_5.setBackgroundResource(R.drawable.mario);break;
                        case 7: button_5.setBackgroundResource(R.drawable.mario);break;
                        case 8: button_5.setBackgroundResource(R.drawable.mario);break;
                        default: button_5.setBackgroundResource(R.drawable.mario); break;
                    }
                }else{
                    switch(gameboard[5]){
                        case 0: button_5.setBackgroundResource(R.drawable.mulde); break;
                        case 1: button_5.setBackgroundResource(R.drawable.bohnenmulde_1);break;
                        case 2: button_5.setBackgroundResource(R.drawable.bohnenmulde_2);break;
                        case 3: button_5.setBackgroundResource(R.drawable.bohnenmulde_3);break;
                        case 4: button_5.setBackgroundResource(R.drawable.bohnenmulde_4);break;
                        case 5: button_5.setBackgroundResource(R.drawable.bohnenmulde_5);break;
                        case 6: button_5.setBackgroundResource(R.drawable.bohnenmulde_6);break;
                        case 7: button_5.setBackgroundResource(R.drawable.bohnenmulde_7);break;
                        case 8: button_5.setBackgroundResource(R.drawable.bohnenmulde_8);break;
                        default: button_5.setBackgroundResource(R.drawable.mulde); break;
                    }
                }
                if(mario[0]==6){
                    switch(gameboard[6]){
                        case 0: button_6.setBackgroundResource(R.drawable.mario); break;
                        case 1: button_6.setBackgroundResource(R.drawable.mario);break;
                        case 2: button_6.setBackgroundResource(R.drawable.mario);break;
                        case 3: button_6.setBackgroundResource(R.drawable.mario);break;
                        case 4: button_6.setBackgroundResource(R.drawable.mario);break;
                        case 5: button_6.setBackgroundResource(R.drawable.mario);break;
                        case 6: button_6.setBackgroundResource(R.drawable.mario);break;
                        case 7: button_6.setBackgroundResource(R.drawable.mario);break;
                        case 8: button_6.setBackgroundResource(R.drawable.mario);break;
                        default: button_6.setBackgroundResource(R.drawable.mario); break;
                    }
                }else{
                    switch(gameboard[6]){
                        case 0: button_6.setBackgroundResource(R.drawable.mulde); break;
                        case 1: button_6.setBackgroundResource(R.drawable.bohnenmulde_1);break;
                        case 2: button_6.setBackgroundResource(R.drawable.bohnenmulde_2);break;
                        case 3: button_6.setBackgroundResource(R.drawable.bohnenmulde_3);break;
                        case 4: button_6.setBackgroundResource(R.drawable.bohnenmulde_4);break;
                        case 5: button_6.setBackgroundResource(R.drawable.bohnenmulde_5);break;
                        case 6: button_6.setBackgroundResource(R.drawable.bohnenmulde_6);break;
                        case 7: button_6.setBackgroundResource(R.drawable.bohnenmulde_7);break;
                        case 8: button_6.setBackgroundResource(R.drawable.bohnenmulde_8);break;
                        default: button_6.setBackgroundResource(R.drawable.mulde); break;
                    }
                }
                if(mario[0]==7){
                    switch(gameboard[7]){
                        case 0: button_7.setBackgroundResource(R.drawable.mario); break;
                        case 1: button_7.setBackgroundResource(R.drawable.mario);break;
                        case 2: button_7.setBackgroundResource(R.drawable.mario);break;
                        case 3: button_7.setBackgroundResource(R.drawable.mario);break;
                        case 4: button_7.setBackgroundResource(R.drawable.mario);break;
                        case 5: button_7.setBackgroundResource(R.drawable.mario);break;
                        case 6: button_7.setBackgroundResource(R.drawable.mario);break;
                        case 7: button_7.setBackgroundResource(R.drawable.mario);break;
                        case 8: button_7.setBackgroundResource(R.drawable.mario);break;
                        default: button_7.setBackgroundResource(R.drawable.mario); break;
                    }
                }else{
                    switch(gameboard[7]){
                        case 0: button_7.setBackgroundResource(R.drawable.mulde); break;
                        case 1: button_7.setBackgroundResource(R.drawable.bohnenmulde_1);break;
                        case 2: button_7.setBackgroundResource(R.drawable.bohnenmulde_2);break;
                        case 3: button_7.setBackgroundResource(R.drawable.bohnenmulde_3);break;
                        case 4: button_7.setBackgroundResource(R.drawable.bohnenmulde_4);break;
                        case 5: button_7.setBackgroundResource(R.drawable.bohnenmulde_5);break;
                        case 6: button_7.setBackgroundResource(R.drawable.bohnenmulde_6);break;
                        case 7: button_7.setBackgroundResource(R.drawable.bohnenmulde_7);break;
                        case 8: button_7.setBackgroundResource(R.drawable.bohnenmulde_8);break;
                        default: button_7.setBackgroundResource(R.drawable.mulde); break;
                    }
                }
                if(mario[0]==8){
                    switch(gameboard[8]){
                        case 0: button_8.setBackgroundResource(R.drawable.mario); break;
                        case 1: button_8.setBackgroundResource(R.drawable.mario);break;
                        case 2: button_8.setBackgroundResource(R.drawable.mario);break;
                        case 3: button_8.setBackgroundResource(R.drawable.mario);break;
                        case 4: button_8.setBackgroundResource(R.drawable.mario);break;
                        case 5: button_8.setBackgroundResource(R.drawable.mario);break;
                        case 6: button_8.setBackgroundResource(R.drawable.mario);break;
                        case 7: button_8.setBackgroundResource(R.drawable.mario);break;
                        case 8: button_8.setBackgroundResource(R.drawable.mario);break;
                        default: button_8.setBackgroundResource(R.drawable.mario); break;
                    }
                }else{
                    switch(gameboard[8]){
                        case 0: button_8.setBackgroundResource(R.drawable.mulde); break;
                        case 1: button_8.setBackgroundResource(R.drawable.bohnenmulde_1);break;
                        case 2: button_8.setBackgroundResource(R.drawable.bohnenmulde_2);break;
                        case 3: button_8.setBackgroundResource(R.drawable.bohnenmulde_3);break;
                        case 4: button_8.setBackgroundResource(R.drawable.bohnenmulde_4);break;
                        case 5: button_8.setBackgroundResource(R.drawable.bohnenmulde_5);break;
                        case 6: button_8.setBackgroundResource(R.drawable.bohnenmulde_6);break;
                        case 7: button_8.setBackgroundResource(R.drawable.bohnenmulde_7);break;
                        case 8: button_8.setBackgroundResource(R.drawable.bohnenmulde_8);break;
                        default: button_8.setBackgroundResource(R.drawable.mulde); break;
                    }
                }
                if(mario[0]==9){
                    switch(gameboard[9]){
                        case 0: button_9.setBackgroundResource(R.drawable.mario); break;
                        case 1: button_9.setBackgroundResource(R.drawable.mario);break;
                        case 2: button_9.setBackgroundResource(R.drawable.mario);break;
                        case 3: button_9.setBackgroundResource(R.drawable.mario);break;
                        case 4: button_9.setBackgroundResource(R.drawable.mario);break;
                        case 5: button_9.setBackgroundResource(R.drawable.mario);break;
                        case 6: button_9.setBackgroundResource(R.drawable.mario);break;
                        case 7: button_9.setBackgroundResource(R.drawable.mario);break;
                        case 8: button_9.setBackgroundResource(R.drawable.mario);break;
                        default: button_9.setBackgroundResource(R.drawable.mario); break;
                    }
                }else{
                    switch(gameboard[9]){
                        case 0: button_9.setBackgroundResource(R.drawable.mulde); break;
                        case 1: button_9.setBackgroundResource(R.drawable.bohnenmulde_1);break;
                        case 2: button_9.setBackgroundResource(R.drawable.bohnenmulde_2);break;
                        case 3: button_9.setBackgroundResource(R.drawable.bohnenmulde_3);break;
                        case 4: button_9.setBackgroundResource(R.drawable.bohnenmulde_4);break;
                        case 5: button_9.setBackgroundResource(R.drawable.bohnenmulde_5);break;
                        case 6: button_9.setBackgroundResource(R.drawable.bohnenmulde_6);break;
                        case 7: button_9.setBackgroundResource(R.drawable.bohnenmulde_7);break;
                        case 8: button_9.setBackgroundResource(R.drawable.bohnenmulde_8);break;
                        default: button_9.setBackgroundResource(R.drawable.mulde); break;
                    }
                }
                if(mario[0]==10){
                    switch(gameboard[10]){
                        case 0: button_10.setBackgroundResource(R.drawable.mario); break;
                        case 1: button_10.setBackgroundResource(R.drawable.mario);break;
                        case 2: button_10.setBackgroundResource(R.drawable.mario);break;
                        case 3: button_10.setBackgroundResource(R.drawable.mario);break;
                        case 4: button_10.setBackgroundResource(R.drawable.mario);break;
                        case 5: button_10.setBackgroundResource(R.drawable.mario);break;
                        case 6: button_10.setBackgroundResource(R.drawable.mario);break;
                        case 7: button_10.setBackgroundResource(R.drawable.mario);break;
                        case 8: button_10.setBackgroundResource(R.drawable.mario);break;
                        default: button_10.setBackgroundResource(R.drawable.mario); break;
                    }
                }else{
                    switch(gameboard[10]){
                        case 0: button_10.setBackgroundResource(R.drawable.mulde); break;
                        case 1: button_10.setBackgroundResource(R.drawable.bohnenmulde_1);break;
                        case 2: button_10.setBackgroundResource(R.drawable.bohnenmulde_2);break;
                        case 3: button_10.setBackgroundResource(R.drawable.bohnenmulde_3);break;
                        case 4: button_10.setBackgroundResource(R.drawable.bohnenmulde_4);break;
                        case 5: button_10.setBackgroundResource(R.drawable.bohnenmulde_5);break;
                        case 6: button_10.setBackgroundResource(R.drawable.bohnenmulde_6);break;
                        case 7: button_10.setBackgroundResource(R.drawable.bohnenmulde_7);break;
                        case 8: button_10.setBackgroundResource(R.drawable.bohnenmulde_8);break;
                        default: button_10.setBackgroundResource(R.drawable.mulde); break;
                    }
                }
                if(mario[0]==11){
                    switch(gameboard[11]){
                        case 0: button_11.setBackgroundResource(R.drawable.mario); break;
                        case 1: button_11.setBackgroundResource(R.drawable.mario);break;
                        case 2: button_11.setBackgroundResource(R.drawable.mario);break;
                        case 3: button_11.setBackgroundResource(R.drawable.mario);break;
                        case 4: button_11.setBackgroundResource(R.drawable.mario);break;
                        case 5: button_11.setBackgroundResource(R.drawable.mario);break;
                        case 6: button_11.setBackgroundResource(R.drawable.mario);break;
                        case 7: button_11.setBackgroundResource(R.drawable.mario);break;
                        case 8: button_11.setBackgroundResource(R.drawable.mario);break;
                        default: button_11.setBackgroundResource(R.drawable.mario); break;
                    }
                }else{
                    switch(gameboard[11]){
                        case 0: button_11.setBackgroundResource(R.drawable.mulde); break;
                        case 1: button_11.setBackgroundResource(R.drawable.bohnenmulde_1);break;
                        case 2: button_11.setBackgroundResource(R.drawable.bohnenmulde_2);break;
                        case 3: button_11.setBackgroundResource(R.drawable.bohnenmulde_3);break;
                        case 4: button_11.setBackgroundResource(R.drawable.bohnenmulde_4);break;
                        case 5: button_11.setBackgroundResource(R.drawable.bohnenmulde_5);break;
                        case 6: button_11.setBackgroundResource(R.drawable.bohnenmulde_6);break;
                        case 7: button_11.setBackgroundResource(R.drawable.bohnenmulde_7);break;
                        case 8: button_11.setBackgroundResource(R.drawable.bohnenmulde_8);break;
                        default: button_11.setBackgroundResource(R.drawable.mulde); break;
                    }
                }



                // button_0.setBackground(buttonBackground);
                // button_1.setBackground(buttonBackground);
                // button_2.setBackground(buttonBackground);
                // button_3.setBackground(buttonBackground);
                // button_4.setBackground(buttonBackground);
                // button_5.setBackground(buttonBackground);
                // button_6.setBackground(buttonBackground);
                // button_7.setBackground(buttonBackground);
                // button_8.setBackground(buttonBackground);
                // button_9.setBackground(buttonBackground);
                // button_10.setBackground(buttonBackground);
                // button_11.setBackground(buttonBackground);

                button_0.setText(Integer.toString(gameboard[0]));
                button_1.setText(Integer.toString(gameboard[1]));
                button_2.setText(Integer.toString(gameboard[2]));
                button_3.setText(Integer.toString(gameboard[3]));
                button_4.setText(Integer.toString(gameboard[4]));
                button_5.setText(Integer.toString(gameboard[5]));
                button_6.setText(Integer.toString(gameboard[6]));
                button_7.setText(Integer.toString(gameboard[7]));
                button_8.setText(Integer.toString(gameboard[8]));
                button_9.setText(Integer.toString(gameboard[9]));
                button_10.setText(Integer.toString(gameboard[10]));
                button_11.setText(Integer.toString(gameboard[11]));
            }
            else {
                switch (position) {
                    case 0:
                        button_0.setText(Integer.toString(value));
                        //button_0.setBackground(android.R.drawable.mu);
                        button_0.setBackgroundResource(android.R.drawable.btn_default);
                        button_0.setBackgroundResource(R.drawable.bohnenmulde_1);
                        if(isGreen == 1)
                            button_0.setBackgroundColor(0xFF00FF00);
                        break;
                    case 1:
                        button_1.setText(Integer.toString(value));
                        button_1.setBackgroundResource(android.R.drawable.btn_default_small);
                        if(isGreen == 1)
                            button_1.setBackgroundColor(0xFF00FF00);
                        break;
                    case 2:
                        button_2.setText(Integer.toString(value));
                        button_2.setBackgroundResource(android.R.drawable.btn_default_small);
                        if(isGreen == 1)
                            button_2.setBackgroundColor(0xFF00FF00);
                        break;
                    case 3:
                        button_3.setText(Integer.toString(value));
                        button_3.setBackgroundResource(android.R.drawable.btn_default_small);
                        if(isGreen == 1)
                            button_3.setBackgroundColor(0xFF00FF00);
                        break;
                    case 4:
                        button_4.setText(Integer.toString(value));
                        button_4.setBackgroundResource(android.R.drawable.btn_default_small);
                        if(isGreen == 1)
                            button_4.setBackgroundColor(0xFF00FF00);
                        break;
                    case 5:
                        button_5.setText(Integer.toString(value));
                        button_5.setBackgroundResource(android.R.drawable.btn_default_small);
                        if(isGreen == 1)
                            button_5.setBackgroundColor(0xFF00FF00);
                        break;
                    case 6:
                        button_6.setText(Integer.toString(value));
                        button_6.setBackgroundResource(android.R.drawable.btn_default_small);
                        if(isGreen == 1)
                            button_6.setBackgroundColor(0xFF00FF00);
                        break;
                    case 7:
                        button_7.setText(Integer.toString(value));
                        button_7.setBackgroundResource(android.R.drawable.btn_default_small);
                        if(isGreen == 1)
                            button_7.setBackgroundColor(0xFF00FF00);
                        break;
                    case 8:
                        button_8.setText(Integer.toString(value));
                        button_8.setBackgroundResource(android.R.drawable.btn_default_small);
                        if(isGreen == 1)
                            button_8.setBackgroundColor(0xFF00FF00);
                        break;
                    case 9:
                        button_9.setText(Integer.toString(value));
                        button_9.setBackgroundResource(android.R.drawable.btn_default_small);
                        if(isGreen == 1)
                            button_9.setBackgroundColor(0xFF00FF00);
                        break;
                    case 10:
                        button_10.setText(Integer.toString(value));
                        button_10.setBackgroundResource(android.R.drawable.btn_default_small);
                        if(isGreen == 1)
                            button_10.setBackgroundColor(0xFF00FF00);
                        break;
                    case 11:
                        button_11.setText(Integer.toString(value));
                        button_11.setBackgroundResource(android.R.drawable.btn_default_small);
                        if(isGreen == 1)
                            button_11.setBackgroundColor(0xFF00FF00);
                        break;
                    case 40:
                        result_A.setText(Integer.toString(value));
                        break;
                    case 50:
                        result_B.setText(Integer.toString(value));
                        break;
                }
            }
            if (position == 2000){
                Log.d("Fragen","Dummy 2000");
                answer_1 = new MyTextView(Bohnenspiel_Mario.this);
                answer_2 = new MyTextView(Bohnenspiel_Mario.this);
                answer_3 = new MyTextView(Bohnenspiel_Mario.this);
                answer_4 = new MyTextView(Bohnenspiel_Mario.this);


                //setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT))

               /* LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                answer_1.setLayoutParams(params);
                answer_1.setMinimumHeight(0);
                answer_1.setMinimumWidth(0);
                answer_1.setTextSize(15);*/



                RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                layoutParams1.leftMargin = 500;
                //layoutParams1.topMargin = 200;
                layoutParams1.bottomMargin = 50;
                //layoutParams1.rightMargin = -250;
                layoutParams1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                layoutParams2.leftMargin = 500;
                //layoutParams2.topMargin = 200;
                layoutParams2.bottomMargin = 100;
                //layoutParams2.rightMargin = -250;
                layoutParams2.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

                RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                layoutParams3.leftMargin = 500;
                //layoutParams3.topMargin = 250;
                layoutParams3.bottomMargin = 150;
                //layoutParams3.rightMargin = -250;
                layoutParams3.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

                RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                layoutParams4.leftMargin = 500;
                //layoutParams4.topMargin = 250;
                layoutParams4.bottomMargin = 200;
                //layoutParams4.rightMargin = -250;
                layoutParams4.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);



                answer_1.setLayoutParams(layoutParams1);
                answer_2.setLayoutParams(layoutParams2);
                answer_3.setLayoutParams(layoutParams3);
                answer_4.setLayoutParams(layoutParams4);




                /***************************************
                 * hier wird DB eingebunden.
                 */

                Cursor resultSet = mydatabase.rawQuery("SELECT id FROM questions ORDER BY RANDOM() LIMIT 1", null);
                resultSet.moveToFirst();
                int questionID = resultSet.getInt(0);
                Log.d("Question", Integer.toString(questionID));
                resultSet.close();

                resultSet = mydatabase.rawQuery("SELECT question FROM questions WHERE id LIKE "+Integer.toString(questionID), null);
                resultSet.moveToFirst();
                question = resultSet.getString(0);
                resultSet.close();

                toolbar.setText(question);

                answer_1.setText("");
                answer_2.setText("");
                answer_3.setText("");
                answer_4.setText("");
                resultSet = mydatabase.rawQuery("SELECT answer FROM answers WHERE question_id LIKE "+Integer.toString(questionID)+" ORDER BY ID",null);
                resultSet.moveToFirst();
                int cursorPosition = 1;
                while (!resultSet.isAfterLast()) {
                    String temp = resultSet.getString(0);
                    switch (cursorPosition){
                        case 1: answer_1.setText(temp); break;
                        case 2: answer_2.setText(temp); break;
                        case 3: answer_3.setText(temp); break;
                        case 4: answer_4.setText(temp); break;
                    }
                    cursorPosition++;
                    resultSet.moveToNext();
                }
                resultSet.close();

                resultSet = mydatabase.rawQuery("SELECT answer FROM answers WHERE question_id LIKE "+Integer.toString(questionID)+" AND correct = 1",null);
                resultSet.moveToFirst();
                rightAnswer = resultSet.getString(0);
                resultSet.close();



                //answer_1.setText("A: Du bist dumm");
                //answer_2.setText("B: 42");
                //answer_3.setText("C: Tololol");
                //answer_4.setText("D: Berlin");

                answer_1.setOnTouchListener(new MyTouchListener());
                answer_2.setOnTouchListener(new MyTouchListener());
                answer_3.setOnTouchListener(new MyTouchListener());
                answer_4.setOnTouchListener(new MyTouchListener());

                answer_1.setBackgroundColor(Color.WHITE);
                //answer_1.setBackgroundResource(R.drawable.schriftrolle2);
                answer_2.setBackgroundColor(Color.WHITE);
                answer_3.setBackgroundColor(Color.WHITE);
                answer_4.setBackgroundColor(Color.WHITE);

                schriftrolle_imageView.setVisibility(View.VISIBLE);
                _root.addView(answer_1);
                _root.addView(answer_2);
                _root.addView(answer_3);
                _root.addView(answer_4);

            }
            if(position==3000){
                Log.d("Fragen","Dummy 3000");
                _root.removeView(answer_1);
                _root.removeView(answer_2);
                _root.removeView(answer_3);
                _root.removeView(answer_4);
                toolbar.setText("");
                schriftrolle_imageView.setVisibility(View.INVISIBLE);
            }
            if(position==3001){
                Log.d("buttonBlink", rightAnswer);
                if(answer_1.getText().equals(rightAnswer)){
                    answer_1.setBackgroundColor(Color.GREEN);
                    _root.removeView(answer_2);
                    _root.removeView(answer_3);
                    _root.removeView(answer_4);

                }
                if(answer_2.getText().equals(rightAnswer)){
                    answer_2.setBackgroundColor(Color.GREEN);
                    _root.removeView(answer_1);
                    _root.removeView(answer_3);
                    _root.removeView(answer_4);
                    Log.d("buttonBlink", "Test");
                }
                if(answer_3.getText().equals(rightAnswer)){
                    answer_3.setBackgroundColor(Color.GREEN);
                    _root.removeView(answer_1);
                    _root.removeView(answer_2);
                    _root.removeView(answer_4);
                    Log.d("buttonBlink", "Test");
                }
                if(answer_4.getText().equals(rightAnswer)){
                    answer_4.setBackgroundColor(Color.GREEN);
                    _root.removeView(answer_2);
                    _root.removeView(answer_3);
                    _root.removeView(answer_1);
                    Log.d("buttonBlink", "Test");
                }
            }

        }
    }
    private class endCard extends AsyncTask<Integer, Integer, Integer>{

        @Override
        protected Integer doInBackground(Integer... params) {
            publishProgress(1);
            return null;
        }
        protected void onProgressUpdate(Integer... progress){
            int won;
            Log.d("Asynctask", "Start");
            AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(Bohnenspiel_Mario.this);
            if(points_A>points_B){
                won = 1;
                dlgAlert.setMessage(getString(R.string.game_over_won));
            }
            else if (points_A == points_B) {
                won = 0;
                dlgAlert.setMessage(getString(R.string.game_over_draw));
            }
            else if (points_B > points_A) {
                won = 0;
                dlgAlert.setMessage(getString(R.string.game_over_lost));
            }
            else {
                dlgAlert.setMessage(getString(R.string.game_over_message));
            }
            dlgAlert.setTitle(getString(R.string.game_over_title));
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            int won = 0;
                            if(points_A>points_B){
                                won = 1;
                            }
                            else if (points_A == points_B) {
                                won = -1;
                            }
                            else if (points_B > points_A) {
                                won = 0;
                            }

                            int level = IntentBundle.getInt("level");
                            SQLiteDatabase mydatabase = openOrCreateDatabase("StatisticDB",MODE_PRIVATE,null);
                            mydatabase.execSQL("INSERT INTO games VALUES ("+Integer.toString(level)+",date('now'),"+Integer.toString(won)+")");
                            finish();
                        }
                    });

            dlgAlert.create().show();
            Log.d("Asynctask", "END");
        }
    }
    private class updateViewsGameThread extends AsyncTask<Integer, Integer, Integer>{

        @Override
        protected Integer doInBackground(Integer... params) {
            publishProgress(1);
            return null;
        }
        protected void onProgressUpdate(Integer... progress){
            drawGameboard();
            drawfinished = true;
        }
    }
    private class questions extends AsyncTask<Integer, Integer, Integer>{

        @Override
        protected Integer doInBackground(Integer... params) {
            publishProgress(1);
            return null;
        }
        protected void onProgressUpdate(Integer... progress){


        }
    }
    private final class MyTouchListener implements View.OnTouchListener {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if(isDragged ==false){
                isDragged = true;
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    _cur = (TextView) view;
                    ClipData data = ClipData.newPlainText("", "");
                    View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                    view.startDrag(data, shadowBuilder, view, 0);
                    view.setVisibility(View.INVISIBLE);
                    return true;
                } else {
                    return false;
                }
            }
            return true;
        }
    }
    class MyDragListener_2 implements View.OnDragListener {
        Drawable enterShape = getResources().getDrawable(R.drawable.shape_droptarget);
        Drawable normalShape = result_A.getBackground();//getResources().getDrawable(R.drawable.shape);

        @Override
        public boolean onDrag(View v, DragEvent event) {
            Log.d("OnDrag",v.toString());
            Log.d("OnDrag",Integer.toString(event.getAction()));
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_LOCATION:
                    float x = event.getX();
                    float y = event.getY();
                    _cur.setX(x-25);
                    _cur.setY(y-25);
                    break;
                case DragEvent.ACTION_DRAG_STARTED:
                    // Do nothing
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    if(v.getId()==R.id.button_points_a) {
                        v.setBackgroundDrawable(enterShape);
                        selectedAnswer = (String)_cur.getText();
                    }

                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    if(v.getId()==R.id.button_points_a) {
                        v.setBackgroundDrawable(normalShape);
                        selectedAnswer = "";
                    }

                    break;
                case DragEvent.ACTION_DROP:


                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    v.setBackgroundDrawable(normalShape);
                    isDragged = false;
                    _cur.post(new Runnable(){
                        @Override
                        public void run() {
                            _cur.setVisibility(View.VISIBLE);
                        }
                    });
                    Log.d("OnDrag Integer", Integer.toString(v.getId()));
                    Log.d("OnDrag Integer",Integer.toString(R.id.button_points_a));
                    if(!selectedAnswer.equals("")){
                        if(selectedAnswer.equals(rightAnswer)){
                            isAnswerRight = true;
                        }
                        else{
                           /* buttonBlink a = new buttonBlink();
                            a.execute();
                            while(!Bohnenspiel_2.buttonBlinkStop){
                                Log.d("ButtonBlink",Boolean.toString(Bohnenspiel_2.buttonBlinkStop));
                            }
                            Bohnenspiel_2.buttonBlinkStop = false;*/
                            isAnswerRight = false;
                        }
                        selectedAnswer = "";
                        pauseBackground = false;
                    }


                    //_cur.setVisibility(View.VISIBLE);
                    //if (dropEventNotHandled(event))
                    //  v.setVisibility(View.VISIBLE);
                    break;

                default:
                    break;
            }
            return true;
        }
    }
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Log.d("onKeyUp","Demo");
            new AlertDialog.Builder(this)
                    .setTitle("Mancala")
                    .setMessage("Do you want to close the game? Any sucess will be lost.")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            return true;
        }

        return false;
    }
}