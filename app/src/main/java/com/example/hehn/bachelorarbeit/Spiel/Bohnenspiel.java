package com.example.hehn.bachelorarbeit.Spiel;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.hehn.bachelorarbeit.KI.KI;
import com.example.hehn.bachelorarbeit.Question;
import com.example.hehn.bachelorarbeit.R;

public class Bohnenspiel extends AppCompatActivity {

    /*
    * *************************************************
    * Variablen Deklaration
    * *************************************************
    * */
    public boolean isPlayerDraw = true;
    public int[] gameboard;
    Intent intent;
    public int points_A = 0;
    public int points_B = 0;
    Button button_1;
    Button button_2;
    Button button_3;
    Button button_4;
    Button button_5;
    Button button_6;
    Button button_7;
    Button button_8;
    Button button_9;
    Button button_10;
    Button button_11;
    Button button_12;
    Button button_result_A;
    Button button_result_B;
    Drawable buttonBackground;
    boolean showQuestion = false;
    public static int status = 1;
    Context context;

    Thread thread = new Thread() {
        @Override
        public void run() {
            while(true) {
                if(Bohnenspiel.status ==0) {
                    Log.d("Thread","Inside");
                    statusCheck stCheck = new statusCheck();
                    stCheck.execute();
                    break;
                }
            }
        }
    };


    /*
    ******************************************************************
        onCreate Methode: Wird beim Erstellen der Aktivity aufgerufen.
        Setzt Layout, initialisiert Views und erstellt relevante Click Listener.
    ******************************************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bohnenspiel);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        button_1 = (Button) findViewById(R.id.button_7);
        button_2 = (Button) findViewById(R.id.button_8);
        button_3 = (Button) findViewById(R.id.button_9);
        button_4 = (Button) findViewById(R.id.button_10);
        button_5 = (Button) findViewById(R.id.button_11);
        button_6 = (Button) findViewById(R.id.button_12);
        button_7 = (Button) findViewById(R.id.button_6);
        button_8 = (Button) findViewById(R.id.button_5);
        button_9 = (Button) findViewById(R.id.button_4);
        button_10 = (Button) findViewById(R.id.button_3);
        button_11 = (Button) findViewById(R.id.button_2);
        button_12 = (Button) findViewById(R.id.button_1);
        button_result_A = (Button) findViewById(R.id.button_points_a);
        button_result_B = (Button) findViewById(R.id.button_points_b);
        buttonBackground =  button_1.getBackground();

        //Click Listener initialisierung
        button_1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                makeDraw(0);
            }
        });
        button_2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                makeDraw(1);
            }
        });
        button_3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                makeDraw(2);
            }
        });
        button_4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                makeDraw(3);
            }
        });
        button_5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                makeDraw(4);
            }
        });
        button_6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                makeDraw(5);
            }
        });
        gameboard = new int[12];
        initializeGameboard();
        context = getApplicationContext();
        thread.start();

    }

    /*************************************************
     * Durchführen eines Zuges;
     * Wird immer von einem onClickListener aufgerufen.
     * @param position: Gibt die ID des Buttons an.
     */
    public void makeDraw(int position){
        disableButton();
        if(isPlayerDraw) {
            isPlayerDraw = false;
            Log.d("makeDraw", "Human");
            if (Bohnenspiel.status==1) {
                gameFlow test = new gameFlow();
                test.execute(position, 0);
            }
            if(gameboard[6] == 0 &&
                    gameboard[7] == 0 &&
                    gameboard[8] == 0 &&
                    gameboard[9] == 0 &&
                    gameboard[10] == 0 &&
                    gameboard[11] == 0
                    ){
                points_A = points_A + gameboard[0]+gameboard[1]+gameboard[2]+gameboard[3]+gameboard[4]+gameboard[5];
                gameboard[0] = 0;
                gameboard[1] = 0;
                gameboard[2] = 0;
                gameboard[3] = 0;
                gameboard[4] = 0;
                gameboard[5] = 0;
                button_1.setText(Integer.toString(gameboard[0]));
                button_2.setText(Integer.toString(gameboard[1]));
                button_3.setText(Integer.toString(gameboard[2]));
                button_4.setText(Integer.toString(gameboard[3]));
                button_5.setText(Integer.toString(gameboard[4]));
                button_6.setText(Integer.toString(gameboard[5]));
                button_7.setText(Integer.toString(gameboard[6]));
                button_8.setText(Integer.toString(gameboard[7]));
                button_9.setText(Integer.toString(gameboard[8]));
                button_10.setText(Integer.toString(gameboard[9]));
                button_11.setText(Integer.toString(gameboard[10]));
                button_12.setText(Integer.toString(gameboard[11]));
                button_result_A.setText(Integer.toString(points_A));
                button_result_B.setText(Integer.toString(points_B));
                Bohnenspiel.status = 0;
                return;
            }
            if(Bohnenspiel.status == 1) {
                KI();
            }
            if(gameboard[0] == 0 &&
                    gameboard[1] == 0 &&
                    gameboard[2] == 0 &&
                    gameboard[3] == 0 &&
                    gameboard[4] == 0 &&
                    gameboard[5] == 0
                    ){
                points_B = points_B + gameboard[6]+gameboard[7]+gameboard[8]+gameboard[9]+gameboard[10]+gameboard[11];
                gameboard[6] = 0;
                gameboard[7] = 0;
                gameboard[8] = 0;
                gameboard[9] = 0;
                gameboard[10] = 0;
                gameboard[11] = 0;
                button_1.setText(Integer.toString(gameboard[0]));
                button_2.setText(Integer.toString(gameboard[1]));
                button_3.setText(Integer.toString(gameboard[2]));
                button_4.setText(Integer.toString(gameboard[3]));
                button_5.setText(Integer.toString(gameboard[4]));
                button_6.setText(Integer.toString(gameboard[5]));
                button_7.setText(Integer.toString(gameboard[6]));
                button_8.setText(Integer.toString(gameboard[7]));
                button_9.setText(Integer.toString(gameboard[8]));
                button_10.setText(Integer.toString(gameboard[9]));
                button_11.setText(Integer.toString(gameboard[10]));
                button_12.setText(Integer.toString(gameboard[11]));
                button_result_A.setText(Integer.toString(points_A));
                button_result_B.setText(Integer.toString(points_B));
                Bohnenspiel.status = 0;
                return;
            }
            if(gameboard[6] == 0 &&
                    gameboard[7] == 0 &&
                    gameboard[8] == 0 &&
                    gameboard[9] == 0 &&
                    gameboard[10] == 0 &&
                    gameboard[11] == 0
                    ){
                points_A = points_A + gameboard[0]+gameboard[1]+gameboard[2]+gameboard[3]+gameboard[4]+gameboard[5];
                gameboard[0] = 0;
                gameboard[1] = 0;
                gameboard[2] = 0;
                gameboard[3] = 0;
                gameboard[4] = 0;
                gameboard[5] = 0;
                button_1.setText(Integer.toString(gameboard[0]));
                button_2.setText(Integer.toString(gameboard[1]));
                button_3.setText(Integer.toString(gameboard[2]));
                button_4.setText(Integer.toString(gameboard[3]));
                button_5.setText(Integer.toString(gameboard[4]));
                button_6.setText(Integer.toString(gameboard[5]));
                button_7.setText(Integer.toString(gameboard[6]));
                button_8.setText(Integer.toString(gameboard[7]));
                button_9.setText(Integer.toString(gameboard[8]));
                button_10.setText(Integer.toString(gameboard[9]));
                button_11.setText(Integer.toString(gameboard[10]));
                button_12.setText(Integer.toString(gameboard[11]));
                button_result_A.setText(Integer.toString(points_A));
                button_result_B.setText(Integer.toString(points_B));
                Bohnenspiel.status = 0;
                return;
            }



        }

    }


    /*******************************************
     * Erstellen des Gameboards
     * Soll mit weniger/mehr als 6 Bohnen gestartet werden, so muss der Value Wert angepasst werden.
     */
    public void initializeGameboard(){
        Bohnenspiel.status = 1;
        points_A = 0;
        points_B = 0;
        int value = 2;
        for (int i=0;i<gameboard.length;i++){
            gameboard[i] = value;
        }
        button_1.setText(Integer.toString(value));
        button_2.setText(Integer.toString(value));
        button_3.setText(Integer.toString(value));
        button_4.setText(Integer.toString(value));
        button_5.setText(Integer.toString(value));
        button_6.setText(Integer.toString(value));
        button_7.setText(Integer.toString(value));
        button_8.setText(Integer.toString(value));
        button_9.setText(Integer.toString(value));
        button_10.setText(Integer.toString(value));
        button_11.setText(Integer.toString(value));
        button_12.setText(Integer.toString(value));


    }
    public void disableButton(){
        button_1.setClickable(false);
        button_2.setClickable(false);
        button_3.setClickable(false);
        button_4.setClickable(false);
        button_5.setClickable(false);
        button_6.setClickable(false);
        button_7.setClickable(false);
        button_8.setClickable(false);
        button_9.setClickable(false);
        button_10.setClickable(false);
        button_11.setClickable(false);
        button_12.setClickable(false);
    }
    public void enableButton(){
        button_1.setClickable(true);
        button_2.setClickable(true);
        button_3.setClickable(true);
        button_4.setClickable(true);
        button_5.setClickable(true);
        button_6.setClickable(true);
        button_7.setClickable(true);
        button_8.setClickable(true);
        button_9.setClickable(true);
        button_10.setClickable(true);
        button_11.setClickable(true);
        button_12.setClickable(true);
    }


    /***********************************************************
     * Implementiert die Künstliche Intelligenz, die gegen den menschlichen Spieler antritt.
     */
    public void KI (){
        int random = 0;
        while(true) {
            while (random < 5 || random > 11 || gameboard[random] == 0) {
                random = (int) (Math.random() * 6);
                random = random + 6;
                //KI a = new KI();
                //random = a.startKI(gameboard.clone(),points_A,points_B);
                Log.d("ZUG KI", Integer.toString(random));
            }
            if (isPlayerDraw == false && gameboard[random] != 0) {
                gameFlow test = new gameFlow();
                test.execute(random, 1);
                isPlayerDraw = true;
                break;
            }
        }
    }
    private class statusCheck extends AsyncTask<Integer, Integer, Integer>{

        @Override
        protected Integer doInBackground(Integer... params) {
                if (Bohnenspiel.status == 0) {
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    publishProgress(1);
                }
            return null;
        }
        protected void onProgressUpdate(Integer... progress){
            Log.d("Asynctask", "Start");
            AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(Bohnenspiel.this);
            if(points_A>points_B){
                dlgAlert.setMessage(getString(R.string.game_over_won));
            }
            else if (points_A == points_B) {
                dlgAlert.setMessage(getString(R.string.game_over_draw));
            }
            else if (points_B > points_A) {
                dlgAlert.setMessage(getString(R.string.game_over_lost));
            }
            else {
                dlgAlert.setMessage(getString(R.string.game_over_message));
            }
            dlgAlert.setTitle(R.string.game_over_title);
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.setCancelable(true);
            dlgAlert.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });

            dlgAlert.create().show();
            Log.d("Asynctask", "END");
        }
    }



    /*************************************************************
     * Für den Spielfluss verantwortlich.
     * in doInBackground wird der Zug berechnet, währenddessen wird bei einem Progress die
     * GUI geupdatet
     */
    private class gameFlow extends AsyncTask<Integer, Integer, Integer> {
        //Status = 1 => Spiel läuft noch.
        //Status = 0 => Spiel ist beendet.
        public int status = 1;
        public int isKI;

        /*************************************************
         * Hier wird der Zug ausgeführt.
         * @param pos
         * @return
         */
        protected Integer doInBackground(Integer... pos) {
            //Test ob ein Spieler keine Steine mehr zur Verfügung hat.
            int position = pos[0];


            //Sind noch genug Steine vorhanden, so kann der Zug ausgeführt werden.

            isKI = pos[1];
            int toDistribute;
            toDistribute = gameboard[position];
            gameboard[position] = 0;
            publishProgress(position,0);
            publishProgress(1000,0);
            for(;toDistribute>0; toDistribute--){
                int temp;
                int temp_value_button;
                position = position + 1;
                if(position > 11){
                    position = 0;
                }
                temp = gameboard[position];
                gameboard[position] = temp+1;
                publishProgress(position,temp+1);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //Change Button Color back
                publishProgress(1000,0);
            }
            //Berechnung der erreichten Punkte
            while(1==1){
                if(gameboard[position]==2 || gameboard[position]==4 || gameboard[position]== 6) {
                    int temp = gameboard[position];
                    gameboard[position] = 0;
                    publishProgress(position, 0,1);
                    //Player Human gets the points
                    if (isKI  == 0) {
                        points_A = points_A + temp;
                        publishProgress(40, points_A);
                        if(position==0){
                            position=11;
                        }
                        else {
                            position--;
                        }
                    }
                    if (isKI  == 1) {
                        points_B = points_B + temp;
                        publishProgress(50, points_B);
                        if(position==0){
                            position=11;
                        }
                        else {
                            position--;
                        }
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    publishProgress(1000, 0);
                }
                else{
                        break;
                    }

            }
            if(isKI == 1) {
                Log.d("FUU","FUU");
                Intent intent = new Intent(Bohnenspiel.this, Question.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        /***********************************************
         * Hier wird die UI geupdatet.
         * @param progress
         */
        protected void onProgressUpdate(Integer... progress) {
            int position = progress[0];
            int value = progress[1];
            int isGreen = 0;
            if(progress.length == 3) {
                try {
                    isGreen = progress[2];
                } catch (Exception e) {
                    Log.d("Error", "Exception abgefangen");
                }
            }

            //Position = 1000 => Zurücksetzen der Felder auf Ausgangszustand.
            //Position 0-11   => Das jeweilige Feld wurde ausgelöst
            //Position 40     => A bekommt die Punkte
            //Position 50     => B bekommt die Punkte
            if(position ==1000){
                button_1.setBackground(buttonBackground);
                button_2.setBackground(buttonBackground);
                button_3.setBackground(buttonBackground);
                button_4.setBackground(buttonBackground);
                button_5.setBackground(buttonBackground);
                button_6.setBackground(buttonBackground);
                button_7.setBackground(buttonBackground);
                button_8.setBackground(buttonBackground);
                button_9.setBackground(buttonBackground);
                button_10.setBackground(buttonBackground);
                button_11.setBackground(buttonBackground);
                button_12.setBackground(buttonBackground);

                button_1.setText(Integer.toString(gameboard[0]));
                button_2.setText(Integer.toString(gameboard[1]));
                button_3.setText(Integer.toString(gameboard[2]));
                button_4.setText(Integer.toString(gameboard[3]));
                button_5.setText(Integer.toString(gameboard[4]));
                button_6.setText(Integer.toString(gameboard[5]));
                button_7.setText(Integer.toString(gameboard[6]));
                button_8.setText(Integer.toString(gameboard[7]));
                button_9.setText(Integer.toString(gameboard[8]));
                button_10.setText(Integer.toString(gameboard[9]));
                button_11.setText(Integer.toString(gameboard[10]));
                button_12.setText(Integer.toString(gameboard[11]));
            }
            else {
                switch (position) {
                    case 0:
                        button_1.setText(Integer.toString(value));
                        button_1.setBackgroundResource(android.R.drawable.btn_default_small);
                        if(isGreen == 1)
                            button_1.setBackgroundColor(0xFF00FF00);
                        break;
                    case 1:
                        button_2.setText(Integer.toString(value));
                        button_2.setBackgroundResource(android.R.drawable.btn_default_small);
                        if(isGreen == 1)
                            button_2.setBackgroundColor(0xFF00FF00);
                        break;
                    case 2:
                        button_3.setText(Integer.toString(value));
                        button_3.setBackgroundResource(android.R.drawable.btn_default_small);
                        if(isGreen == 1)
                            button_3.setBackgroundColor(0xFF00FF00);
                        break;
                    case 3:
                        button_4.setText(Integer.toString(value));
                        button_4.setBackgroundResource(android.R.drawable.btn_default_small);
                        if(isGreen == 1)
                            button_4.setBackgroundColor(0xFF00FF00);
                        break;
                    case 4:
                        button_5.setText(Integer.toString(value));
                        button_5.setBackgroundResource(android.R.drawable.btn_default_small);
                        if(isGreen == 1)
                            button_5.setBackgroundColor(0xFF00FF00);
                        break;
                    case 5:
                        button_6.setText(Integer.toString(value));
                        button_6.setBackgroundResource(android.R.drawable.btn_default_small);
                        if(isGreen == 1)
                            button_6.setBackgroundColor(0xFF00FF00);
                        break;
                    case 6:
                        button_7.setText(Integer.toString(value));
                        button_7.setBackgroundResource(android.R.drawable.btn_default_small);
                        if(isGreen == 1)
                            button_7.setBackgroundColor(0xFF00FF00);
                        break;
                    case 7:
                        button_8.setText(Integer.toString(value));
                        button_8.setBackgroundResource(android.R.drawable.btn_default_small);
                        if(isGreen == 1)
                            button_8.setBackgroundColor(0xFF00FF00);
                        break;
                    case 8:
                        button_9.setText(Integer.toString(value));
                        button_9.setBackgroundResource(android.R.drawable.btn_default_small);
                        if(isGreen == 1)
                            button_9.setBackgroundColor(0xFF00FF00);
                        break;
                    case 9:
                        button_10.setText(Integer.toString(value));
                        button_10.setBackgroundResource(android.R.drawable.btn_default_small);
                        if(isGreen == 1)
                            button_10.setBackgroundColor(0xFF00FF00);
                        break;
                    case 10:
                        button_11.setText(Integer.toString(value));
                        button_11.setBackgroundResource(android.R.drawable.btn_default_small);
                        if(isGreen == 1)
                            button_11.setBackgroundColor(0xFF00FF00);
                        break;
                    case 11:
                        button_12.setText(Integer.toString(value));
                        button_12.setBackgroundResource(android.R.drawable.btn_default_small);
                        if(isGreen == 1)
                            button_12.setBackgroundColor(0xFF00FF00);
                        break;
                    case 40:
                        button_result_A.setText(Integer.toString(value));
                        break;
                    case 50:
                        button_result_B.setText(Integer.toString(value));
                        break;
                }
            }

        }

        @Override
        protected void onPostExecute(Integer integer) {

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        int extrapoints;
        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            extrapoints= 0;
        } else {
            extrapoints= extras.getInt("points");
        }

        Log.d("onResumeBohnenspiel",Integer.toString(extrapoints));
        points_A = points_A + extrapoints;
        button_result_A.setText(Integer.toString(points_A));
        enableButton();
    }
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }
    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.ic_face_black_48dp), 10, 10, null);
    }
    




}

