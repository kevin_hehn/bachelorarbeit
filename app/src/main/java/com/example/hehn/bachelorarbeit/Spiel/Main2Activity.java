package com.example.hehn.bachelorarbeit.Spiel;

import android.content.ClipData;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.hehn.bachelorarbeit.R;

public class Main2Activity extends AppCompatActivity {

    TextView answer_1;
    TextView answer_2;
    TextView answer_3;
    TextView answer_4;
    TextView solution;
    ViewGroup _root;
    TextView _cur;
    boolean isDragged = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        _root = (ViewGroup)findViewById(R.id.root);
        answer_1 = new TextView(this);//(TextView) findViewById(R.id.Trolo);
        answer_2 = (TextView) findViewById(R.id.Trolo2);
        answer_1.setText("Dummy");


        RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(150, 50);
        layoutParams1.leftMargin = 200;
        layoutParams1.topMargin = 200;
        layoutParams1.bottomMargin = -250;
        layoutParams1.rightMargin = -250;


        answer_1.setLayoutParams(layoutParams1);

        //answer_1.setOnTouchListener(new MyTouchListener());
        //answer_1.setOnDragListener(new MyDragListener2());

        //_root.setClipChildren(false);
       // _root.addView(answer_1);
        //_root.addView(answer_2);

        answer_1.setOnTouchListener(new MyTouchListener());
        answer_2.setOnTouchListener(new MyTouchListener());
        _root.setOnDragListener(new MyDragListener());
        _root.addView(answer_1);


    }

    private final class MyTouchListener implements View.OnTouchListener {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if(isDragged ==false){
                isDragged = true;
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    _cur = (TextView) view;
                    ClipData data = ClipData.newPlainText("", "");
                    View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                    view.startDrag(data, shadowBuilder, view, 0);
                    view.setVisibility(View.INVISIBLE);
                    return true;
                } else {
                    return false;
                }
            }
            return true;
        }
    }
    class MyDragListener implements View.OnDragListener {
        //Drawable enterShape = getResources().getDrawable(R.drawable.shape_droptarget);
        //Drawable normalShape = getResources().getDrawable(R.drawable.shape);

        @Override
        public boolean onDrag(View v, DragEvent event) {
            Log.d("OnDrag",v.toString());
            Log.d("OnDrag",Integer.toString(event.getAction()));
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_LOCATION:
                    float x = event.getX();
                    float y = event.getY();
                    Log.d("X",Float.toString(x));
                    Log.d("Y", Float.toString(y));
                    _cur.setX(x);
                    _cur.setY(y);
                    break;
                case DragEvent.ACTION_DRAG_STARTED:
                    // Do nothing
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:

                    break;
                case DragEvent.ACTION_DRAG_EXITED:

                    break;
                case DragEvent.ACTION_DROP:


                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    isDragged = false;
                    _cur.setVisibility(View.VISIBLE);
                    //if (dropEventNotHandled(event))
                      //  v.setVisibility(View.VISIBLE);
                    break;

                default:
                    break;
            }
            return true;
        }
    }
}
