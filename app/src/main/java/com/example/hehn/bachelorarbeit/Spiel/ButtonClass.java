package com.example.hehn.bachelorarbeit.Spiel;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;

import com.example.hehn.bachelorarbeit.Question;

/**
 * Created by Hehn on 03.01.2016.
 */
public class ButtonClass extends BaseAdapter{
    private Context mContext;
    String[] Answers;
    int right_answer;

    public ButtonClass(Context c, String[] a,int r){
        mContext = c;
        Answers = a.clone();
        right_answer = r;
    }
    @Override
    public int getCount() {
        return Answers.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        Button button;
        if(convertView ==null){
            button = new Button(mContext);
            //button.setLayoutParams(new GridView.LayoutParams(85, 85));
            button.setLayoutParams(new GridView.LayoutParams(GridView.LayoutParams.FILL_PARENT,
                    GridView.LayoutParams.WRAP_CONTENT));;
            button.setPadding(8, 8, 8, 8);
        }
        else{
            button = (Button)convertView;
        }
        button.setEnabled(true);
        button.setClickable(true);
        button.setText(Answers[position]);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("onItemClick", Answers[position]);
                if (position == right_answer) {
                    Log.d("onItemClick", "Richtig");
                    Question.right = true;
                    Question.answered = true;

                    //Intent resumeBohnenspiel = new Intent(v.getContext(),Bohnenspiel.class);
                    //resumeBohnenspiel.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    //resumeBohnenspiel.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    //v.getContext().startActivity(resumeBohnenspiel);
                } else {
                    Question.right = false;
                    Question.answered = true;
                    Log.d("OnItemClick", "Falsch");
                }
            }
        });
        return button;
    }

}
