package com.example.hehn.bachelorarbeit.Spiel;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

/**
 * Created by Hehn on 17.01.2016.
 */
public class MyRelativLayout  extends RelativeLayout {


    public MyRelativLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MyRelativLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyRelativLayout(Context context) {
        super(context);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        Log.d("Relative", Integer.toString(event.getAction()));
        return false;
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // Never allow swiping to switch between pages
        return false;
    }
}
